$(document).ready(function(){
    $('.delete_camp').on('click', function(){
        var href = $(this).data('value');
        var confirmCheck = confirm('Are you sure you want to delete this Campaign');

        if(confirmCheck == true){
            window.location.href = href;
        }
    });
});
