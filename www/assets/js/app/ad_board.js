$(document).ready(function(){
    var slides_count = [];
    $('.sidesshow').cycle();
    $('.sidesshow').on( 'cycle-after', function(event, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag) {
        var vid = $('.cycle-slide-active.use').find('.vid');
        if(typeof vid[0] !== 'undefined'){
            vid[0].play();
            $('.cycle-slide-active.use .base_item').css('margin-top', '100%');
            var ind = 0;
            $('.cycle-slide-active.use .base_item').each(function(){
                $(this).delay(400*ind).animate({'margin-top':'0px'}, 1300);
                ind++;
            });
        }
        var campaignHash = $('.cycle-slide-active.use').data('hash');
        if(typeof slides_count[campaignHash] == 'undefined'){
            slides_count[campaignHash] = 1;
        }else{
            slides_count[campaignHash] = slides_count[campaignHash] + 1;
        }
    });
    $('.sidesshow').on( 'cycle-before', function(event, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag) {
        var finishedSLide = $('.cycle-slide-active.use');
        if(typeof finishedSLide[0] !== 'undefined'){
            var campaignHash = $('.cycle-slide-active.use').data('hash');
            var campaignId = $('.cycle-slide-active.use').data('value');
            var taxiId = $('#taxi_id').val();
            $.ajax({
                type: "POST",
                url: "ad_board/count_slide",
                data: {taxi_id: taxiId, campaign_id: campaignId, increament: slides_count[campaignHash]},
                success: function(rtn){
                    console.log(rtn);
                    if(rtn == 'success'){
                        slides_count[campaignHash] = slides_count[campaignHash] - slides_count[campaignHash];
                        console.log(slides_count);
                    }
                }
            });
            var vid = $('.cycle-slide-active.use').find('.vid');
            if(typeof vid[0] !== 'undefined'){
                vid[0].pause();
                vid[0].currentTime = 0;
            }
            var ind = 0;
            $('.cycle-slide-active.use .base_item').each(function(){
                $(this).delay(400*ind).animate({'margin-top':'100%'}, 1300);
                ind++;
            });
        }
    });

    $('.sidesshow').height($(window).height());
});
