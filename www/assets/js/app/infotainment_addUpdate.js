$(document).ready(function(){

    $('.change_vid').on('click', function(){
        $('#video').click();
        // $('#video_tg').addClass('hidden');
        // $(this).addClass('hidden');
    });
    // Code to get duration of audio /video file before upload - from: http://coursesweb.net/

    //register canplaythrough event to #audio element to can get duration
    var f_duration =0;  //store duration
    document.getElementById('video_tg').addEventListener('canplaythrough', function(e){
      //add duration in the input field #duration
      f_duration = Math.round(e.currentTarget.duration);
      document.getElementById('duration').value = f_duration;
      URL.revokeObjectURL(obUrl);
    });

    //when select a file, create an ObjectURL with the file and add it in the #audio element
    var obUrl;
    document.getElementById('video').addEventListener('change', function(e){
      var file = e.currentTarget.files[0];
      //check file extension for audio/video type
      if(file.name.match(/\.(avi|mp3|mp4|mpeg|ogg)$/i)){
        obUrl = URL.createObjectURL(file);
        document.getElementById('video_tg').setAttribute('src', obUrl);
        $('.change_vid').text('Change video');
      }
    });

});
