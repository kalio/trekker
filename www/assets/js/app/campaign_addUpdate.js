$(document).ready(function(){

    $( ".datepicker" ).datepicker({
        "dateFormat" : 'yy-mm-dd'
    });

    $('.toggle_owner').on('click', function(){
        var target = $(this).data('value');
        $('.owner_switch').addClass('hidden');
        $('.toggle_owner').addClass('hidden');
        $('.'+target).removeClass('hidden');
    });

    if($('#user_id').val() != ''){
        $('.owner_switch').addClass('hidden');
        $('.toggle_owner').addClass('hidden');
        $('.owner_name').removeClass('hidden');
    }

    $('.change_vid').on('click', function(){
        $('#campaign_video').click();
        // $('#video_tg').addClass('hidden');
        // $(this).addClass('hidden');
    });

    $('.owner_search').keyup(function(){
        $('#results').removeClass('hidden');
        var searchField = $('.owner_search').val();
        var regex = new RegExp(searchField, "i");
        var output = '<div class="row">';
        var count = 1;
        if(searchField != ''){
            if(Usersdata != {}){
                $.each(Usersdata, function(key, val){
                    var bothnames = val.first_name+' '+val.last_name;
                    if ((val.first_name.search(regex) != -1) || (val.last_name.search(regex) != -1) || (val.email.search(regex) != -1)
                        || (bothnames.search(regex) != -1)) {
                        output += '<div class="col-md-6 search_option" data-value="'+val.id+'" data-fname="'+val.first_name+'" data-lname="'+val.last_name+'">';
                        output += '<label>' + val.first_name + ' '+val.last_name+' - '+val.email+'</label>';
                        output += '</div>';
                        if(count%2 == 0){
                            output += '</div><div class="row">'
                        }
                        count++;
                    }
                });
            }
            output += '</div>';
            $('#results').html(output);
        }else{
            $('#results').html('');
        }
    });

    $(document).off('click').on('click', '.search_option', function(){
        var id = $(this).data('value');
        var first_name = $(this).data('fname');
        var last_name = $(this).data('lname');
        $('.owner_search').val(first_name+' '+last_name);
        $('#user_id').val(id);
        $('#results').addClass('hidden');
    });

    // Code to get duration of audio /video file before upload - from: http://coursesweb.net/

    //register canplaythrough event to #audio element to can get duration
    var f_duration =0;  //store duration
    document.getElementById('video_tg').addEventListener('canplaythrough', function(e){
      //add duration in the input field #duration
      f_duration = Math.round(e.currentTarget.duration);
      document.getElementById('duration').value = f_duration;
      URL.revokeObjectURL(obUrl);
    });

    //when select a file, create an ObjectURL with the file and add it in the #audio element
    var obUrl;
    document.getElementById('campaign_video').addEventListener('change', function(e){
      var file = e.currentTarget.files[0];
      //check file extension for audio/video type
      if(file.name.match(/\.(avi|mp3|mp4|mpeg|ogg)$/i)){
        obUrl = URL.createObjectURL(file);
        document.getElementById('video_tg').setAttribute('src', obUrl);
        $('.change_vid').text('Change video');
      }
    });

    $('.add_route').on('click', function(){
        var index = $('.routes .route_box').length;
        var route_box_element = $('.route_hold .route_box');
        $('div.routes').append(route_box_element.clone().attr('id', 'route_'+index));
        $('div.routes #route_'+index+' .route').attr('id', 'select_route_'+index);
        $('div.routes #route_'+index+' .route').attr('name', 'routes['+index+'][route]');
        $('div.routes #route_'+index+' .route').data('index', index);
        $('div.routes #route_'+index+' .no_taxi').attr('id', 'select_taxi_'+index);
        $('div.routes #route_'+index+' .no_taxi').attr('name', 'routes['+index+'][no_taxi]');
        $('div.routes #route_'+index+' .remove_route').data('index', index);
    });

    $('div.routes ').off('click').on('click', '.remove_route', function(){
        var target = $(this).data('index');
        var value = $('#select_route_'+target+' option:selected').val();
        if(jQuery.inArray(value, select_options) !== -1){
            select_options.splice(select_options.indexOf(value), 1);
        }
        reviseRouteOption(0);
        $('#route_'+target).remove();
        if($('.add_route').hasClass('hidden')){
            $('.add_route').removeClass('hidden');
        }
    });

    $('div.routes').off('change').on('change', '.select_route', function(){
        var target = $(this).data('index');
        var route_id = $(this).val();
        $('#route_id_'+target).val(route_id);
        $('#select_taxi_'+target).find('option').remove().end().append('<option value="">Select Number of Taxis</option>');
        if(route_id != ''){
            getRouteTaxisOptions(target, route_id);
            reviseRouteOption(target);
        }
    });
    if($('.select_route').length > 0){
        $('.select_route').not('.route').each(function(){
            if($(this).val() != null && typeof $(this).val() !== 'undefined'){
                var target = $(this).data('index');
                var route_id = $(this).val();
                $('#route_id_'+target).val(route_id);
                $('#select_taxi_'+target).find('option').remove().end().append('<option value="">Select Number of Taxis</option>');
                if(route_id != ''){
                    getRouteTaxisOptions(target, route_id);
                    reviseRouteOption(target);
                }
            }
        });
    }
});
function getRouteTaxisOptions(target, route_id){
    $.ajax({
        url: "/campaigns/getRouteTaxis",
        type: "post",
        data: {route_id: route_id},
        success: function (response) {
            response = JSON.parse(response);
            var value = $('#select_taxi_'+target).data('value');
            if(response.length > 0){
                for(var i = 1; i <= response.length; i++){
                    if(value !='' && i == value){
                        var selected = 'selected="selected"';
                    }else{
                        var selected = '';
                    }
                    $('#select_taxi_'+target).append('<option value="'+i+'" '+selected+'>'+i+' Taxi(s)</option>');
                }
                $('#select_taxi_'+target).attr('readonly', false);
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {

        }

    });
}

var select_options = [];
function reviseRouteOption(target){
    $('.routes .select_route').each(function(ele){
        if($(this).val() != '' && $(this).val() != null){
            if(select_options[target] !== 'undefined'){
                select_options.splice(target, 1);
            }
            select_options[target] = $(this).val();
        }
    });

    $('.select_route option').each(function(){

        if(jQuery.inArray($(this).val(), select_options) !== -1){
            $(this).hide().prop('disabled', true).attr('disabled', true);
        }else{
            $(this).show().prop('disabled', false).attr('disabled', false);
        }
    });

    if($('#select_route_'+target+' option:disabled').length == $('#select_route_'+target+' option').length - 1){
        $('.add_route').addClass('hidden');
    }
}
