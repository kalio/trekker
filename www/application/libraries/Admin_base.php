<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_base extends Session_bot{

	function __construct()
	{
		parent::__construct();

        $is_admin_present = $this->User_Model->findManyWhere(array('role'=>'Admin', 'status' => 'Active'));
		if(empty($is_admin_present['success'])){
            // add default admin user
            $newAdmin = new stdClass();
            $newAdmin->email = 'admin@trekker.com';
            $newAdmin->password = md5('testpass');
            $newAdmin->role = 'Admin';
            $newAdmin->status = 'Active';
	        $ret = $this->User_Model->save($newAdmin);
        }

	}

}
?>
