<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Custom_validation extens CI_Form_validation{

    	/**
    	 * Is Unique in collection
    	 *
    	 * Check if the input value doesn't already exist
    	 * in the specified database field.
    	 *
    	 * @param	string	$str
    	 * @param	string	$field
    	 * @return	bool
    	 */
    	public function is_unique_in_collection($str, $field)
    	{
    		sscanf($field, '%[^.].%[^.]', $table, $field);
    		$collection = $table.'_Model';

            $query = $this->$collection->findFirstWhere(array($field => $str));
            if(count($query) == 0) {
                return true;
            }else{
                return false;
            }
    	}

    	// --------------------------------------------------------------------
}

}
