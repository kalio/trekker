<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Session_bot extends Trekker_Controller{

	function __construct()
	{
		parent::__construct();
	}

    public function isLoggedin() {
		return (bool)$this->session->userdata('loggedin');
	}

    public function logUserIn($post = array()){
        $filter = $post;
        $filter['status'] = 'Active';
        $filter['role'] = 'Admin';
        $AuthUser = $this->User_Model->findFirstWhere($filter);
        if($AuthUser['success']){
            $AuthUser['data']['loggedin'] = true;
            $this->session->set_userdata($AuthUser['data']);
            return true;
        }

        return false;
    }
}
?>
