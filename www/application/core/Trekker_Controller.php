<?php
class Trekker_Controller extends CI_Controller
{
	public $view_data = array();

    function __construct(){
        parent::__construct();


        $this->load->helper('upload');

        $this->load->model('User_Model');
        $this->load->model('Campaign_Model');
        $this->load->model('Route_Model');
        $this->load->model('Taxi_Model');
        $this->load->model('Infotainment_Model');
        $this->load->model('RouteTaxiCampaign_Model');

        $usersArray = $this->User_Model->findManyWhere(array('status' => 'Active', 'role' => array('$ne' => 'Admin')));
        $newArray = array();
        if(!empty($usersArray['data'])){
            for($x = 0; $x < count($usersArray['data']); $x++){
                $usersArray['data'][$x]['id'] = (string)$usersArray['data'][$x]['_id'];
                $newArray[$usersArray['data'][$x]['id']] = $usersArray['data'][$x];
            }
        }

        $this->view_data['usersArray'] = $newArray;
    }
}
?>
