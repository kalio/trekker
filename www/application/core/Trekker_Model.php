<?php
class Trekker_Model extends CI_Model
{

	private $CI;
	private $config = array();
	private $param = array();
	private $activate;
	private $connect;
	private $db;
    private $collection;
    private $result;
	private $hostname;
	private $port;
	private $database;
	private $username;
	private $password;
	private $debug;
	private $write_concerns;
	private $journal;
	private $selects = array();
	private $updates = array();
	private $wheres	= array();
	private $limit	= 999999;
	private $offset	= 0;
	private $sorts	= array();
	private $return_as = 'array';
	public $benchmark = array();

    function __construct($param = array())
    {

        require_once 'vendor/autoload.php';
        if ( ! class_exists('MongoDB\Driver\Manager') && ! class_exists('MongoClient'))
        {
            show_error("The MongoDB PECL extension has not been installed or enabled", 500);
        }
        $this->CI =& get_instance();
        $this->CI->load->config('mongo_db');
        $this->config = $this->CI->config->item('mongo_db');
        $this->param = $param;
        $this->connect();
    }

    private function connect()
	{
		$this->prepare();
		try
		{
			$dns = "mongodb://{$this->hostname}:{$this->port}/{$this->database}";
			if(isset($this->config[$this->activate]['no_auth']) == TRUE && $this->config[$this->activate]['no_auth'] == TRUE)
			{
				$options = array();
			}
			else
			{
				$options = array('username'=>$this->username, 'password'=>$this->password);
			}
			$this->connect = new MongoDB\Client($dns, $options);
			$this->db = $this->connect->{$this->database};
            if(!empty($this->param['collection'])){
                $this->collection = $this->db->{$this->param['collection']};
            }else{
				return "Unable to connect to 'Undefined' Collection";
            }
		}
		catch (MongoDB\Driver\Execption\ConnectionException $e)
		{
			if(isset($this->debug) == TRUE && $this->debug == TRUE)
			{
				show_error("Unable to connect to MongoDB: {$e->getMessage()}", 500);
			}
			else
			{
				show_error("Unable to connect to MongoDB", 500);
			}
		}
	}

    private function prepare()
	{
		if(is_array($this->param) && count($this->param) > 0 && isset($this->param['activate']) == TRUE)
		{
			$this->activate = $this->param['activate'];
		}
		else if(isset($this->config['active']) && !empty($this->config['active']))
		{
			$this->activate = $this->config['active'];
		}else
		{
			show_error("MongoDB configuration is missing.", 500);
		}

		if(isset($this->config[$this->activate]) == TRUE)
		{
			if(empty($this->config[$this->activate]['hostname']))
			{
				show_error("Hostname missing from mongodb config group : {$this->activate}", 500);
			}
			else
			{
				$this->hostname = trim($this->config[$this->activate]['hostname']);
			}

			if(empty($this->config[$this->activate]['port']))
			{
				show_error("Port number missing from mongodb config group : {$this->activate}", 500);
			}
			else
			{
				$this->port = trim($this->config[$this->activate]['port']);
			}

			if(isset($this->config[$this->activate]['no_auth']) == FALSE
			   && empty($this->config[$this->activate]['username']))
			{
				show_error("Username missing from mongodb config group : {$this->activate}", 500);
			}
			else
			{
				$this->username = trim($this->config[$this->activate]['username']);
			}

			if(isset($this->config[$this->activate]['no_auth']) == FALSE
			   && empty($this->config[$this->activate]['password']))
			{
				show_error("Password missing from mongodb config group : {$this->activate}", 500);
			}
			else
			{
				$this->password = trim($this->config[$this->activate]['password']);
			}

			if(empty($this->config[$this->activate]['database']))
			{
				show_error("Database name missing from mongodb config group : {$this->activate}", 500);
			}
			else
			{
				$this->database = trim($this->config[$this->activate]['database']);
			}

			if(empty($this->config[$this->activate]['db_debug']))
			{
				$this->debug = FALSE;
			}
			else
			{
				$this->debug = $this->config[$this->activate]['db_debug'];
			}

			if(empty($this->config[$this->activate]['write_concerns']))
			{
				$this->write_concerns = 1;
			}
			else
			{
				$this->write_concerns = $this->config[$this->activate]['write_concerns'];
			}

			if(empty($this->config[$this->activate]['journal']))
			{
				$this->journal = TRUE;
			}
			else
			{
				$this->journal = $this->config[$this->activate]['journal'];
			}

			if(empty($this->config[$this->activate]['return_as']))
			{
				$this->return_as = 'array';
			}
			else
			{
				$this->return_as = $this->config[$this->activate]['return_as'];
			}
		}
		else
		{
			show_error("mongodb config group :  <strong>{$this->activate}</strong> does not exist.", 500);
		}
	}

    public function findFirstWhere($filter = array()){
        if(!empty($filter)){
			try{
	            $result = $this->collection->findOne($filter);
	            if(!empty($result)){
	                $this->result['success'] = true;
	                $this->result['data'] = (array)$result;
	            }else{
	                $this->result['success'] = false;
					$this->result['data'] = '';
	            }
			}catch (MongoDB\Driver\Exception\AuthenticationException $e){
				$this->createDbUser();
			}
        }else{
            $this->result['success'] = false;
            $this->result['errors'] = array("Bad request, the filter array is empty");
        }

        return $this->result;
    }

    public function findManyWhere($filter = array(), $options = array()){
        if(is_array($filter)){
			try{
	            $result = $this->collection->find($filter, $options);
				$result = iterator_to_array($result);
	            if($result != null && !empty($result)){
	                $this->result['success'] = true;
	                $this->result['data'] = $result;
	            }else{
	                $this->result['success'] = false;
					$this->result['data'] = '';
	            }
			}catch (MongoDB\Driver\Exception\AuthenticationException $e){
				$this->createDbUser();
			}
        }else{
            $this->result['success'] = false;
            $this->result['errors'] = array("Bad request, the filter array is empty");
        }

        return $this->result;
    }

    public function deleteWhere($filter = array()){
        if(!empty($filter)){
			try{
	            $result = $this->collection->deleteOne($filter);
	            if(!empty($result) && $result->getDeletedCount() > 0){
	                $this->result['success'] = true;
	            }else{
	                $this->result['success'] = false;
	                $this->result['errors'] = array("Bad request, record was not found");
	            }
			}catch (MongoDB\Driver\Exception\AuthenticationException $e){
				$this->createDbUser();
			}
        }else{
            $this->result['success'] = false;
            $this->result['errors'] = array("Bad request, the filter array is empty");
        }

        return $this->result;
    }

    public function save($data = array()){
		try{
	        if(!empty($data)){
	            if(is_array($data)){
	                // $this->result = $this->collection->insertMany($data);
	                // TO-DO
	                return false;
	            }else{
					$data->created = new \MongoDB\BSON\UTCDateTime();
					$data->unique_hash = $this->getUniqueHash();
	                $result = $this->collection->insertOne($data);
	                if(!empty($result) && $result->getInsertedCount() > 0){
	                    $this->result['success'] = true;
	                    $this->result['data'] = array('id' => (String)$result->getInsertedId());
	                }
	            }
	        }else{
	            $this->result['success'] = false;
	            $this->result['errors'] = array("Bad request, the data array is empty");
	        }

	        return $this->result;
		}catch (MongoDB\Driver\Exception\AuthenticationException $e){
			$this->createDbUser();
		}
    }

    public function update($data = array(), $id, $many = ''){
		try{
	        if(!is_array($id) && !empty($id)){
	            $where = array('_id' => new MongoDB\BSON\ObjectID($id));
	        }
	        if(!empty($data) && !empty($where)){
	            if($many){
	                // $this->result = $this->collection->updateMany($where, array('$set' => $data));
	                // TO-DO
	                return false;
	            }else{
			        $data['updated'] = new \MongoDB\BSON\UTCDateTime();
	                $result = $this->collection->updateOne($where, array('$set' => $data));
	                if(!empty($result) && $result->getModifiedCount() > 0){
	                    $this->result['success'] = true;
	                    $this->result['data'] = array('id' => $id);
	                }else{
	                    $this->result['success'] = false;
	                    $this->result['errors'] = array("Bad request, record was not found");
	                }
	            }
	        }else{
	            $this->result['success'] = false;
	            $this->result['errors'] = array("Bad request, the data array is empty");
	        }

	        return $this->result;
		}catch (MongoDB\Driver\Exception\AuthenticationException $e){
			$this->createDbUser();
		}
    }

	public function aggregate($filter = array(), $group = array(), $options = array()){
		if(is_array($filter)){
			try{
	            $result = $this->collection->aggregate(array((object)$filter, (object)$group), $options);
				$result = iterator_to_array($result);
	            if($result != null && !empty($result)){
	                $this->result['success'] = true;
	                $this->result['data'] = $result;
	            }else{
	                $this->result['success'] = false;
					$this->result['data'] = '';
	            }
			}catch (MongoDB\Driver\Exception\AuthenticationException $e){
				$this->createDbUser();
			}
        }else{
            $this->result['success'] = false;
            $this->result['errors'] = array("Bad request, the filter array is empty");
        }

        return $this->result;
	}

    public function is_unique_in_collection( $field,$str){
		$query = $this->findFirstWhere(array($field => $str));
        if(empty($query['success'])) {
            return true;
        }else{
            return false;
        }
    }

	private function getUniqueHash($length = 50){
		$token = "";
		$codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
		$codeAlphabet.= "0123456789";
		$max = strlen($codeAlphabet); // edited

		for ($i=0; $i < $length; $i++) {
		$token .= $codeAlphabet[rand(0, $max-1)];
		}

		return $token;
	}

	private function createDbUser(){
		// $dns = "mongodb://localhost:27017";
  		// $mongo = new MongoDB\Client($dns, array());
		// $db = $mongo->{'admin'};
		// $collection = $db->{'system.users'};
		//
		// // insert the user - note that the password gets hashed as 'username:mongo:password'
		// // set readOnly to true if user should not have insert/delete privs
		// $collection = $db->selectCollection("system.users");
		// $collection->insertOne(array('user' => $this->username, 'pwd' => md5($this->password), 'readOnly' => false));
	}
}
