<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Infotainment extends Admin_base {

	private $errors = array();

	function __construct(){
		parent::__construct();
        if(!$this->isLoggedin()){
            redirect('login');
        }
		$this->view_data['page_tile'] = 'Infotainment Management';
	}

	public function index()	{
		$this->view_data['page'] = 'infotainment';
		$infotainments = $this->Infotainment_Model->findManyWhere(array('status' => 'Active'));
		$this->view_data['infotainments'] = !empty($infotainments['data'])?$infotainments['data']:array();
		$this->load->view('infotainment_list', $this->view_data);
	}

    public function addUpdate(){
		$this->view_data['page'] = 'infotainment_addUpdate';
		$infotainmentHash = !empty($_GET['infotainment_hash'])?$_GET['infotainment_hash']:'';
		if(!empty($infotainmentHash)){
			$this->view_data['page_sub_title'] = 'Update Infotainment';
		}else{
			$this->view_data['page_sub_title'] = 'Add Infotainment';
		}

		if(_isPost()){
			$post = $this->view_data['post'] = $this->input->post();
			if(!empty($post)){
				$infotainment_validate = $this->Infotainment_Model->collection_fields;
				$this->form_validation->set_rules($infotainment_validate);
				if(!empty($_FILES['video']) && $this->form_validation->run() == TRUE){
					$video_link = '';
					if(empty($infotainmentHash)){
						// New Infotainment
						if(!$this->Infotainment_Model->is_unique_in_collection('title', $post['title'])){
							$this->view_data['validation_errors'] = array('title' => 'An Infotainment with title "'.$post['title'].'" already exists.');
						}elseif(empty($_FILES['video']) || ($_FILES['video']['error'] != 0)){
							$this->view_data['validation_errors'] = array('video' => 'There was an error with your infotainment video upload, please try again');
						}else{
							$uploaded_content = upload($_FILES['video'], 'infotainments');
							$data = array(
			                    "title" => $post["title"],
								"video" => $uploaded_content["data"]["file"],
			                    "video_length" => $post["video_length"],
								"status" => "Active"
			                 );
							 $rtn = $this->Infotainment_Model->save((Object)$data);
							 if($rtn['success'] == true){
								 redirect('infotainment');
							 }else{
		 						$this->view_data['validation_errors'] = array('video' => 'An error occured. Please try again');
							}
						}
					}else{
						// Update Infotainment
						$infotainment = $this->Infotainment_Model->findFirstWhere(array('unique_hash' => $infotainmentHash));
						$infotainment_id = !empty($infotainment['data']['_id'])?(String)$infotainment['data']['_id']:'';
						$post = array_merge($infotainment['data'], $post);
						if(!empty($infotainment['success']) && !empty($infotainment_id)){
							if(!empty($_FILES['video']) && ($_FILES['video']['error'] == 0)){
								$uploaded_content = upload($_FILES['video'], 'infotainments');
								if(!empty($uploaded_content["data"]) && !empty($uploaded_content["data"]["file"]) && !empty($infotainment['data']['video']) && is_file(ASSPATH.'infotainments\\'.$infotainment['data']['video'])){
									unlink(ASSPATH.'infotainments\\'.$infotainment['data']['video']);
								}
								$video_link = !empty($uploaded_content["data"]["file"])?$uploaded_content["data"]["file"]:(!empty($post["video"])?$post["video"]:'');
							}else{
								if(!empty($_FILES['video']) && !empty($_FILES['video']['name']) && ($_FILES['video']['error'] != 0)){
									$this->view_data['validation_errors'] = array('video' => 'An error occured uploading your infotainment video. Please try again');
								}
								$video_link = !empty($post["video"])?$post["video"]:'';
							}
							// if(($_FILES['video']['error'] == 0) && !empty($infotainment['data']['campaign_video']) && is_file(ASSPATH.'infotainments\\'.$infotainment['data']['video'])){
							// 	unlink(ASSPATH.'infotainments\\'.$infotainment['data']['video']);
							// }
							// $uploaded_content = upload($_FILES['video'], 'infotainments');
							$data = array(
								"video" => $video_link,
								"video_length" => $post["video_length"],
			                    "title" => $post["title"]
							 );
							 $rtn = $this->Infotainment_Model->update($data, $infotainment_id);
							 if($rtn['success'] == true){
								 $this->view_data['update_success'] = true;
								 $infotainment = $this->Infotainment_Model->findFirstWhere(array('unique_hash' => $infotainmentHash, 'status' => 'Active'));
				 				if(!empty($infotainment['success'])){
				 					redirect('infotainment');
				 				}
							 }else{
								$this->view_data['validation_errors'] = array('video' => 'An error occured. Please try again');
							}
						}else{
							redirect('infotainment');
						}
					}
				}else{
					if(empty($_FILES['video'])){
						$this->view_data['validation_errors'] = array('video' => 'Please select a video for to upload');
					}
				}
			}
		}else{
			if(!empty($infotainmentHash)){
				$infotainment = $this->Infotainment_Model->findFirstWhere(array('unique_hash' => $infotainmentHash));
				if(!empty($infotainment['success'])){
					$this->view_data['infotainment'] = $infotainment['data'];
				}else{
					redirect('infotainment');
				}
			}
		}
		$this->load->view('infotainment_addUpdate', $this->view_data);
    }

	public function delete(){
		$infotainmentHash = !empty($_GET['infotainment_hash'])?$_GET['infotainment_hash']:'';
		if(!empty($infotainmentHash)){
			$infotainment = $this->Infotainment_Model->findFirstWhere(array('unique_hash' => $infotainmentHash, 'status' => 'Active'));
			if(!empty($infotainment['success'])){
				if(!empty($infotainment['data']['video']) && is_file(ASSPATH.'infotainments\\'.$infotainment['data']['video'])){
					unlink(ASSPATH.'infotainments\\'.$infotainment['data']['video']);
				}

				$rtn = $this->Infotainment_Model->deleteWhere(array('_id' => $infotainment['data']['_id']));
				if(!empty($rtn['success'])){
					redirect('infotainment');
				}
			}else{

			}
		}else{
			redirect('infotainment');
		}
	}
}
