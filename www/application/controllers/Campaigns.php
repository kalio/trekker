<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Campaigns extends Admin_base {

	private $errors = array();

	function __construct(){
		parent::__construct();
        if(!$this->isLoggedin()){
            redirect('login');
        }
		$this->view_data['page_tile'] = 'Campaign Management';
	}

	public function index()	{
		$this->view_data['page'] = 'campaign';
		$campaigns = $this->Campaign_Model->findManyWhere(array());
		if(!empty($campaigns['success'])) foreach($campaigns['data'] as &$campaign){
			$campaign_taxis_data = $this->RouteTaxiCampaign_Model->findManyWhere(array('campaign_id' => (string)$campaign['_id']));
			$campaign['count'] = 0;
			if(!empty($campaign_taxis_data['success']))foreach($campaign_taxis_data['data'] as $campaign_taxi_data){
				if(!empty($campaign_taxi_data['count'])){
					$campaign['count'] += $campaign_taxi_data['count'];
				}
			}
		}
		$this->view_data['campaigns'] = !empty($campaigns['success'])?$campaigns['data']:array();
		$this->load->view('campaigns_list', $this->view_data);
	}

    public function addUpdate(){
		$this->view_data['page'] = 'campaign_addUpdate';
		$campaignHash = !empty($_GET['campaign_hash'])?$_GET['campaign_hash']:'';
		if(!empty($campaignHash)){
			$this->view_data['page_sub_title'] = 'Update Campaign';

		}else{
			$this->view_data['page_sub_title'] = 'Add Campaign';
		}

		$routes = $this->Route_Model->findManyWhere(array('status' => 'Active'));
		$this->view_data['routes'] = !empty($routes['success'])?$routes['data']:array();

		if(_isPost()){
			$errors = array();
			$post = $this->view_data['post'] = $this->input->post();
			if(!empty($post)){
				if(!empty($post['user_id'])){
					$post['owner'] = $post['user_id'];
				}else{
					$user_validate = $this->User_Model->collection_fields;
					$this->form_validation->set_rules($user_validate);
					if($this->form_validation->run() == TRUE){
						if($this->User_Model->is_unique_in_collection('email', $post['email'])){
				            $newUser = (Object)$post;
				            $newUser->password = md5('password');
				            $newUser->status = 'Active';
							$creatUser_return = $this->User_Model->save($newUser);
							if(!empty($creatUser_return['success'])){
								$post['owner'] = $creatUser_return['data']['id'];
							}
						}else{
							$this->view_data['validation_errors'] = array('email' => 'A user with email '.$post['email'].' already exists.');
						}
					}
				}

				$campaign_validate = $this->Campaign_Model->collection_fields;
				$this->form_validation->set_rules($campaign_validate);
				if(!empty($post['owner']) && $this->form_validation->run() == TRUE){
					// Allocate Slote
					$slot_allocation;
					if(!empty($post['video_length'])){
						$slot_allocation = ceil(($post['video_length']/30));
					}

					// Check Slot availablitiy in available taxis on selected routes
					if(!empty($post['routes'])) foreach($post['routes'] as $route_key => $route){
						$selected_route = $this->Route_Model->findFirstWhere(array('_id' => new MongoDB\BSON\ObjectId($route['route'])));
						if(!empty($selected_route['success'])){
							// get route taxis
							$taxi_count = 0;
			                $route_taxis = $this->Taxi_Model->findManyWhere(array('route_id' => $route['route']));
							if(!empty($route_taxis['success'])) foreach ($route_taxis['data'] as $route_taxi){
								$totalSlots = 0;
								$alltaxiCampaigns = $this->RouteTaxiCampaign_Model->findManyWhere(array('rout_id' => $route['route'], 'taxi_id' => array('$elemMatch' => array('$in' => array((string)$route_taxi['_id']))), 'status' => 'Active'));
								if(!empty($alltaxiCampaigns['success'])){
									foreach($alltaxiCampaigns['data'] as $taxicampaign){
										$tcampaign = $this->Campaign_Model->findFirstWhere(array('_id' => new MongoDB\BSON\ObjectId($taxicampaign['campaign_id'])));
										if(!empty($tcampaign['success'])){
											$totalSlots += $tcampaign['slot_allocation'];
										}
									}
								}

								if(($totalSlots + $slot_allocation) <= 20){
									// check if route taxi campaign exist
									if(!empty($campaignHash)){
										$rtcampaign = $this->Campaign_Model->findFirstWhere(array('unique_hash' => $campaignHash, 'status' => array('$ne' => 'Ended')));
										if(!empty($rtcampaign['success'])){
											$checkroutetaxicampaign = $this->RouteTaxiCampaign_Model->findFirstWhere(array('campaign_id' => (string)$rtcampaign['data']['_id'], 'route_id' => $route['route'], 'taxi_id' => array('$elemMatch' => array('$in' => array((string)$route_taxi['_id']))), 'status' => 'Active'));
											if(empty($checkroutetaxicampaign['success'])){
												$post['routes'][$route_key]['route_taxi_data'][] = array(
													'campaign_id' => (string)$rtcampaign['data']['_id'],
													'route_id' => $route['route'],
													'taxi_id' => (string)$route_taxi['_id'],
													'count' => 0,
													'status' => 'Active'
												);
												$taxi_count += 1;
											}
										}else{
											$errors = 'An error occured: The campaign you are trying to update was not found, or might have ended, please refresh the page a nd try again.';
										}
									}else{
										$post['routes'][$route_key]['route_taxi_data'][] = array(
											'campaign_id' => '',
											'route_id' => $route['route'],
											'taxi_id' => (string)$route_taxi['_id'],
											'count' => 0,
											'status' => 'Active'
										);
										$taxi_count += 1;
									}
								}
								if($taxi_count == $route['no_taxi']){
									break;
								}
							}
						}
					}

					if(empty($errors)){
						if(empty($campaignHash)){
							// New Campaign
							if(!$this->Campaign_Model->is_unique_in_collection('campaign_title', $post['campaign_title'])){
								$this->view_data['validation_errors'] = array('campaign_title' => 'A Campaign with title "'.$post['campaign_title'].'" already exists.');
							}elseif(empty($_FILES['campaign_video']) || ($_FILES['campaign_video']['error'] != 0)){
								$this->view_data['validation_errors'] = array('campaign_video' => 'There was an error with your campaign video upload, please try again');
							}else{
								$uploaded_ad = upload($_FILES['campaign_video'], 'ads');
								$data = array(
				                    "owner" => $post["owner"],
				                    "campaign_title" => $post["campaign_title"],
									"campaign_video" => $uploaded_ad["data"]["file"],
				                    "video_length" => $post["video_length"],
				                    "campaign_duration" => $post["campaign_duration"],
									"start_date" => $post["start_date"],
				                    "slot_allocation" => !empty($slot_allocation)?$slot_allocation:1,
				                    "links" => !empty($post["links"])?$post["links"]:array(),
									"status" => "Pending"
				                 );
								 $rtn = $this->Campaign_Model->save((Object)$data);
								 if($rtn['success'] == true){
									 $campaign = $this->Campaign_Model->findFirstWhere(array('_id' => new MongoDB\BSON\ObjectId($rtn['data']['id'])));
									 if(!empty($campaign['success']) && !empty($post['routes']))foreach($post['routes'] as $campaign_route){
										 if(!empty($campaign_route['route_taxi_data']))foreach($campaign_route['route_taxi_data'] as $campaignroutetaxi_data){
											 $campaignroutetaxi_data['campaign_id'] = (string)$campaign['data']['_id'];
											 $this->RouteTaxiCampaign_Model->save((object)$campaignroutetaxi_data);
										 }
									 }
									 redirect('campaigns');
								 }else{
			 						$this->view_data['validation_errors'] = array('campaign_video' => 'An error occured. Please try again');
								}
							}
						}else{
							// Update Campagin
							$campaign = $this->Campaign_Model->findFirstWhere(array('unique_hash' => $campaignHash, 'status' => array('$ne' => 'Ended')));
							$campaign_id = !empty($campaign['data']['_id'])?(String)$campaign['data']['_id']:'';
							$post = array_merge($campaign['data'], $post);
							if(!empty($campaign['success']) && !empty($campaign_id)){
								$campain_video_link = '';
								if(!empty($_FILES['campaign_video']) && ($_FILES['campaign_video']['error'] == 0)){
									$uploaded_ad = upload($_FILES['campaign_video'], 'ads');
									if(!empty($uploaded_ad["data"]) && !empty($uploaded_ad["data"]["file"]) && !empty($campaign['data']['campaign_video']) && is_file(ASSPATH.'ads\\'.$campaign['data']['campaign_video'])){
										unlink(ASSPATH.'ads\\'.$campaign['data']['campaign_video']);
									}
									$campain_video_link = !empty($uploaded_ad["data"]["file"])?$uploaded_ad["data"]["file"]:(!empty($post["campaign_video"])?$post["campaign_video"]:'');
								}else{
									if(!empty($_FILES['campaign_video']) && !empty($_FILES['campaign_video']['name']) && ($_FILES['campaign_video']['error'] != 0)){
								   		$this->view_data['validation_errors'] = array('campaign_video' => 'An error occured uploading your campaign video. Please try again');
									}
									$campain_video_link = !empty($post["campaign_video"])?$post["campaign_video"]:'';
								}
								$not_started = !empty($post["start_date"])?(strtotime(date('Y-m-d')) < strtotime($post["start_date"])):false;
								$data = array(
									"campaign_video" => $campain_video_link,
									"video_length" => $post["video_length"],
				                    "campaign_duration" => $post["campaign_duration"],
									"slot_allocation" => !empty($slot_allocation)?$slot_allocation:1,
									"start_date" => (!empty($campaign['data']['status']) && $campaign['data']['status'] == 'Pending')?$post["start_date"]:(!empty($campaign['data']['start_date'])?$campaign['data']['start_date']:''),
									"links" => !empty($post["links"])?$post["links"]:array()
								 );
								 $rtn = $this->Campaign_Model->update($data, $campaign_id);
								 if($rtn['success'] == true){
									$this->view_data['update_success'] = true;
									$campaign = $this->Campaign_Model->findFirstWhere(array('unique_hash' => $campaignHash));
									if(!empty($campaign['success']) && !empty($post['routes']))foreach($post['routes'] as $campaign_route){
									 	if(!empty($campaign_route['route_taxi_data']))foreach($campaign_route['route_taxi_data'] as $campaignroutetaxi_data){
									 		$campaignroutetaxi_data['campaign_id'] = (string)$campaign['data']['_id'];
									 		$campaignroutetaxi_check = $this->RouteTaxiCampaign_Model->findFirstWhere(array('campaign_id' => $campaignroutetaxi_data['campaign_id'], 'route_id' => $campaignroutetaxi_data['route_id'], 'taxi_id' => $campaignroutetaxi_data['taxi_id']));
											if(empty($campaignroutetaxi_check['success'])){
												$this->RouteTaxiCampaign_Model->save((object)$campaignroutetaxi_data);
											}
								 		}
								 	}
									if(empty($this->view_data['validation_errors'])){
				 						redirect('campaigns');
									}
								 }else{
									$this->view_data['validation_errors'] = array('campaign_video' => 'An error occured. Please try again');
								}
							}
						}
					}else{
						$this->view_data['validation_errors'] = array('campaign_title' => $errors);
					}
				}else{
					if(empty($_FILES['campaign_video'])){
						$this->view_data['validation_errors'] = array('campaign_video' => 'Please select a video for the campaign');
					}
				}
			}
		}
		if(!empty($campaignHash)){
			$campaign = $this->Campaign_Model->findFirstWhere(array('unique_hash' => $campaignHash, 'status' => array('$ne' => 'Inactive')));
			if(!empty($campaign['success'])){
				$owner = $this->User_Model->findFirstWhere(array('_id' => new \MongoDB\BSON\ObjectID($campaign['data']['owner']), 'status' => 'Active'));
				$taxiAggregation_data = array(
					'campaign_id' => '$campaign_id',
					'route_id' => '$route_id'
				);
				$campaignroutestaxis = array();
				$taxi_aggregation = $this->RouteTaxiCampaign_Model->aggregate(array('$match' => array('campaign_id' => (string)$campaign['data']['_id'], 'status' => 'Active')), array('$group' => array('_id' => $taxiAggregation_data,'taxi_count' => array('$sum' => 1))));
				if(!empty($taxi_aggregation['success'])) foreach ($taxi_aggregation['data'] as $key => $taxi_agg){
					$campaignroutetaxis = (array)$taxi_agg['_id'];
					$campaignroutetaxis['taxi_count'] = $taxi_agg['taxi_count'];
					$campaignroutetaxis['route_taxis_data'] = array();
					$campaignroutestaxis_data = $this->RouteTaxiCampaign_Model->findManyWhere(array('campaign_id' => $campaignroutetaxis['campaign_id'], 'route_id' => $campaignroutetaxis['route_id'], 'status' => 'Active'));
					if(!empty($campaignroutestaxis_data['success'])){
						$campaignroutetaxis['route_taxis_data'] = (array)$campaignroutestaxis_data['data'];
					}
					$campaignroutetaxis['taxi_data'] = array();
					$route_taxis = $this->Taxi_Model->findManyWhere(array('route_id' => $campaignroutetaxis['route_id']));
					if(!empty($route_taxis['success'])){
						$campaignroutetaxis['taxi_data'] = (array)$route_taxis['data'];
					}
					$route = $this->Route_Model->findFirstWhere(array('_id' => new MongoDB\BSON\ObjectId($campaignroutetaxis['route_id'])));
					$campaignroutetaxis['route_name'] = !empty($route['success'])?$route['data']['route']:'';
					array_push($campaignroutestaxis, $campaignroutetaxis);
				}
				$this->view_data['campaign'] = $campaign['data'];
				$this->view_data['campaignroutestaxis'] = !empty($campaignroutestaxis)?$campaignroutestaxis:array();
				$this->view_data['owner'] = !empty($owner['success'])?$owner['data']:array();
			}else{
				redirect('campaigns');
			}
		}else{
			redirect('campaigns');
		}
		$this->load->view('campaigns_addUpdate', $this->view_data);
    }

    public function report(){
		$this->load->view('campaigns_report');
    }

	public function delete(){
		$campaignHash = !empty($_GET['campaign_hash'])?$_GET['campaign_hash']:'';
		if(!empty($campaignHash)){
			$campaign = $this->Campaign_Model->findFirstWhere(array('unique_hash' => $campaignHash, 'status' => array('$ne' => 'Active')));
			if(!empty($campaign['success'])){
				if(!empty($campaign['data']['campaign_video']) && is_file(ASSPATH.'ads\\'.$campaign['data']['campaign_video'])){
					unlink(ASSPATH.'ads\\'.$campaign['data']['campaign_video']);
				}

				$rtn = $this->Campaign_Model->deleteWhere(array('_id' => $campaign['data']['_id']));
				if(!empty($rtn['success'])){
					$rtn = $this->RouteTaxiCampaign_Model->deleteWhere(array('campaign_id' => (string)$campaign['data']['_id']));
					redirect('campaigns');
				}
			}else{
				redirect('campaigns');
			}
		}else{
			redirect('campaigns');
		}
	}

	public function getRouteTaxis(){
		if(_isPost()){
			$post =  $this->input->post();
			if(!empty($post['route_id'])){
                $route_taxis = $this->Taxi_Model->findManyWhere(array('route_id' => $post['route_id']));
				$route_taxis = !empty($route_taxis['success'])?$route_taxis['data']:array();
                print_r(json_encode($route_taxis));
			}
		}
	}
}
