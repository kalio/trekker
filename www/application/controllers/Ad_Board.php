<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ad_board extends Session_bot {
	function __construct(){
		parent::__construct();
	}

	public function index()
	{
		$view_data = array();
		$taxi_hash = !empty($_GET['taxi'])?$_GET['taxi']:'';
		if(!empty($taxi_hash)){
			$taxi = $this->Taxi_Model->findFirstWhere(array('unique_hash' => $taxi_hash, 'status' => 'Active'));
			if(!empty($taxi['success'])){
				// get Campaigns
				$taxi_campaigns = $this->getTaxiCampaigns((string)$taxi['data']['_id']);
				// get infotainments
				$infotainments = $this->Infotainment_Model->findManyWhere(array('status' => 'Active'));
				$show_cycle = $this->getCampaignInfotainmentCycle($taxi_campaigns, !empty($infotainments['data'])?$infotainments['data']:array());
				// _d($show_cycle, 1);
				$view_data['campaigns'] = $show_cycle;
				$view_data['taxi'] = $taxi['data'];
			}else{
				redirect('welcome');
			}
		}
		$this->load->view('ad_board', $view_data);
	}

	public function count_slide(){
		if(_isPost()){
			$post = $this->input->post();
			if(!empty($post['campaign_id']) && !empty($post['taxi_id']) && !empty($post['increament'])){
				$taxi = $this->Taxi_Model->findFirstWhere(array('_id' => new MongoDB\BSON\ObjectId($post['taxi_id']), 'status' => 'Active'));
				if(empty($taxi['success'])){
					redirect('welcome');
				}
				$campaign = $this->Campaign_Model->findFirstWhere(array('_id' => new MongoDB\BSON\ObjectId($post['campaign_id']), 'status' => array('$ne' => 'Ended')));
				if(!empty($campaign['success'])){
					$taxicampaign = $this->RouteTaxiCampaign_Model->findFirstWhere(array('taxi_id' => (string)$taxi['data']['_id'], 'campaign_id' => (string)$campaign['data']['_id']));
					if(!empty($taxicampaign['success'])){
						if(!empty($taxicampaign['data']['count'])){
							$newCount = $taxicampaign['data']['count'] + $post['increament'];
						}else{
							$newCount = $post['increament'];
						}

						$rtn = $this->RouteTaxiCampaign_Model->update(array("count" => $newCount), (string)$taxicampaign['data']['_id']);
						if(!empty($rtn['success'])){
							echo 'success';
						}
					}
				}
			}
		}else{
			redirect('ad_board');
		}
	}

	private function getCampaignInfotainmentCycle($campaings = array(), $infotainments = array()){
		$cycle = array();
		if(!empty($campaings) || !empty($infotainments)){
			$count = 1;
			for($i = 0; $i < 36; $i++){
				if($count <= 5){
					if(!empty($campaings)){
						$selected_campaign_key = array_rand($campaings);
						$cycle[$i] = $campaings[$selected_campaign_key];
						unset($campaings[$selected_campaign_key]);
					}
					$count++;
				}else{
					if(!empty($infotainments)){
						$selected_infotainment_key = array_rand($infotainments);
						$cycle[$i] = (array)$infotainments[$selected_infotainment_key];
						unset($infotainments[$selected_infotainment_key]);
					}
					$count = 1;
				}
			}
		}
		return !empty($cycle)?array_values($cycle):array();
	}

	private function getTaxiCampaigns($taxi_id){
		$taxi_campaigns = $this->RouteTaxiCampaign_Model->findManyWhere(array('taxi_id' => $taxi_id);
		if(!empty($taxi_campaigns['success']) && !empty($taxi_campaigns['data'])){
			foreach($taxi_campaigns['data'] as &$taxi_campaign){
				$taxi_campaign_data = $this->Campaign_Model->findFirstWhere(array('_id' => new MongoDB\BSON\ObjectId($taxi_campaign['campaign_id']), 'status' => array('$ne' => 'Ended')));
				if(!empty($taxi_campaign_data['data'])){
					$taxi_campaign_data = $taxi_campaign_data['data'];
					$current_date = date('Y-m-d');

					$campaign_id = !empty($taxi_campaign_data['_id'])?(String)$taxi_campaign_data['_id']:'';
					$status = '';						// start campaign
					if($taxi_campaign_data['status'] == 'Pending' && !empty($taxi_campaign_data['start_date']) && strtotime($taxi_campaign_data['start_date']) >= strtotime($current_date)){
						$status = 'Active';
					}else{
						$status = $taxi_campaign_data['status'];
					}

					// end campaign
					if(!empty($taxi_campaign_data['start_date']) && strtotime($taxi_campaign_data['start_date']." + ".$taxi_campaign_data['campaign_duration']." weeks") <= strtotime($current_date)){
						$status = 'Ended';
					}
					if($status != $taxi_campaign_data['status']){
						$data = array( "status" => $status );
						$rtn = $this->Campaign_Model->update($data, $campaign_id);
						if(!empty($rtn['success'])){
							$taxi_campaign_data['status'] = $status;
						}
					}
					$taxi_campaign_data['taxi_data'] = (array)$taxi_campaign;
					$taxi_campaign = $taxi_campaign_data;
				}
			}

			return $taxi_campaigns['data'];
		}else{
			return array();
		}
	}
}
