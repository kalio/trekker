<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends Session_bot {
	function __construct(){
		parent::__construct();

        if($this->isLoggedin()){
            $this->session->sess_destroy();
        }
	}

	public function index()
	{
		$this->load->view('login');
        if(_isPost()){
            $post = $this->input->post();
            if(!empty($post)){
                if(!empty($post['password'])){
                    $post['password'] = md5($post['password']);
                }
                if($this->logUserIn($post)){
                    redirect('admin');
                }
            }else{

            }
        }
	}
}
