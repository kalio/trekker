<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Routes extends Admin_base {
	function __construct(){
		parent::__construct();
	}

	public function index()
	{
		$this->view_data['page'] = 'routes';
		$routes = $this->Route_Model->findManyWhere(array());
        if($routes['success'])foreach($routes['data'] as $index => $route){
            $route_taxis = $this->Taxi_Model->findManyWhere(array('route_id' => (string)$route['_id']));
            $routes['data'][$index]['taxis'] = !empty($route_taxis['success'])?$route_taxis['data']:array();
        }
		$this->view_data['routes'] = !empty($routes['data'])?$routes['data']:array();
		$this->load->view('routes_list', $this->view_data);
	}

    public function addUpdate(){
		$this->view_data['page'] = 'routes_addUpdate';
		$routeHash = !empty($_GET['route_hash'])?$_GET['route_hash']:'';
		if(!empty($routeHash)){
			$this->view_data['page_sub_title'] = 'Update Route';
		}else{
			$this->view_data['page_sub_title'] = 'Add Route';
		}

		if(_isPost()){
			$post = $this->view_data['post'] = $this->input->post();
			if(!empty($post)){
				$route_validate = $this->Route_Model->collection_fields;
				$this->form_validation->set_rules($route_validate);
				if($this->form_validation->run() == TRUE){
                    $data = array(
                        "route" => $post["route"],
                        "status" => "Active"
                    );
                    if(!empty($post['id'])){
                        // update
                        $rtn = $this->Route_Model->update($data, $post['id']);
                    }else{
                        // add
                        $rtn = $this->Route_Model->save((Object)$data);
                    }

                    if($rtn['success'] == true){
                        redirect('routes');
                    }else{
                       $this->view_data['validation_errors'] = array('route' => 'An error occured. Please try again');
                    }
				}
			}
		}else{
			if(!empty($routeHash)){
				$route = $this->Route_Model->findFirstWhere(array('unique_hash' => $routeHash, 'status' => array('$ne' => 'Inactive')));
                if(!empty($route['success'])){
                    $route_taxis = $this->Taxi_Model->findManyWhere(array('route_id' => (string)$route['data']['_id']));
                    $route['data']['taxis'] = !empty($route_taxis['success'])?$route_taxis['data']:array();
                    $this->view_data['route'] = $route['data'];
				}else{
					redirect('routes');
				}
			}
		}
		$this->load->view('routes_addUpdate', $this->view_data);
    }

	public function delete(){
		$routeHash = !empty($_GET['route_hash'])?$_GET['route_hash']:'';
		if(!empty($routeHash)){
			$route = $this->Route_Model->findFirstWhere(array('unique_hash' => $routeHash, 'status' => 'Active'));
			if(!empty($route['success'])){
				$rtn = $this->Route_Model->deleteWhere(array('_id' => $route['data']['_id']));
				if(!empty($rtn['success'])){
                    $route_taxis = $this->Taxi_Model->findManyWhere(array('route_id' => (string)$route['data']['_id']));
                    if(!empty($route_taxis['success'])) foreach($route_taxis['data'] as $taxi){
                        $taxi_id = (string)$taxi['_id'];
                        $data = array('route_id' => '');
                        $this->Taxi_Model->update($data, $taxi_id);
                    }
					redirect('routes');
				}
			}
		}else{
			redirect('routes');
		}
	}
}
