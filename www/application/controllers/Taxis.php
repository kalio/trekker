<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Taxis extends Admin_base {
	function __construct(){
		parent::__construct();
	}

	public function index()
	{
		$this->view_data['page'] = 'taxis';
		$taxis = $this->Taxi_Model->findManyWhere(array());
        if($taxis['success'])foreach($taxis['data'] as $index => $taxi){
            $taxi_route = $this->Route_Model->findFirstWhere(array('_id' => new \MongoDB\BSON\ObjectID($taxi['route_id'])));
            $taxis['data'][$index]['taxi_route'] = !empty($taxi_route['success'])?$taxi_route['data']:array();
        }
		$this->view_data['taxis'] = !empty($taxis['data'])?$taxis['data']:array();
		$this->load->view('taxis_list', $this->view_data);
	}

    public function addUpdate(){
		$this->view_data['page'] = 'taxis_addUpdate';
		$taxiHash = !empty($_GET['taxi_hash'])?$_GET['taxi_hash']:'';
		if(!empty($taxiHash)){
			$this->view_data['page_sub_title'] = 'Update Taxi';
		}else{
			$this->view_data['page_sub_title'] = 'Add Taxi';
		}

        $routes = $this->Route_Model->findManyWhere(array('status' => 'Active'));
        $this->view_data['routes'] = !empty($routes['success'])?$routes['data']:array();

		if(_isPost()){
			$post = $this->view_data['post'] = $this->input->post();
			if(!empty($post)){
                $rtn = array();
				$taxi_validate = $this->Taxi_Model->collection_fields;
				$this->form_validation->set_rules($taxi_validate);
				if($this->form_validation->run() == TRUE){
                    $data = array(
                        "taxi_no" => $post["taxi_no"],
                        "license" => strtoupper($post["license"]),
                        "route_id" => $post["route_id"],
                        "status" => "Active"
                    );
                    if(!empty($post['id'])){
                        // update
                        $rtn = $this->Taxi_Model->update($data, $post['id']);
                    }else{
                        // add
                        if(!$this->Taxi_Model->is_unique_in_collection('license', $post['license'])){
							$this->view_data['validation_errors'] = array('license' => 'A Taxi with license number "'.$post['license'].'" already exists.');
						}else{
                            $rtn = $this->Taxi_Model->save((Object)$data);
                        }
                    }

                    if(!empty($rtn['success']) && $rtn['success'] == true){
                        redirect('taxis');
                    }
				}
			}
		}else{
			if(!empty($taxiHash)){
				$taxi = $this->Taxi_Model->findFirstWhere(array('unique_hash' => $taxiHash, 'status' => array('$ne' => 'Inactive')));
                if(!empty($taxi['success'])){
                    $taxi_route = $this->Route_Model->findManyWhere(array('route_id' => (string)$taxi['data']['_id']));
                    $taxi['data']['route'] = !empty($taxi_route['success'])?$taxi_route['data']:array();
                    $this->view_data['taxi'] = $taxi['data'];
				}else{
					redirect('taxis');
				}
			}
		}
        $all_taxi = $this->Taxi_Model->findManyWhere(array());
        $this->view_data['new_taxi_number'] = !empty($all_taxi['success'])?'TK-'.str_pad(count($all_taxi['data']) + 1, 5, '0', STR_PAD_LEFT):'TK-00001';
		$this->load->view('taxis_addUpdate', $this->view_data);
    }

	public function delete(){
		$taxiHash = !empty($_GET['taxi_hash'])?$_GET['taxi_hash']:'';
		if(!empty($taxiHash)){
			$taxi = $this->Taxi_Model->findFirstWhere(array('unique_hash' => $taxiHash, 'status' => 'Active'));
			if(!empty($taxi['success'])){
				$rtn = $this->Taxi_Model->deleteWhere(array('_id' => $taxi['data']['_id']));
				if(!empty($rtn['success'])){
					redirect('taxis');
				}
			}
		}else{
			redirect('taxis');
		}
	}
}
