<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class Consolidate extends Session_bot {

	private $errors = array();

	function __construct(){
		parent::__construct();
        if(!$this->isLoggedin()){
            redirect('login');
        }
	}

	public function index()	{
		$taxi_hash = !empty($_GET['taxi'])?$_GET['taxi']:'';
		if(!empty($taxi_hash)){
			$taxi = $this->Taxi_Model->findFirstWhere(array('unique_hash' => $taxi_hash, 'status' => 'Active'));
			if(!empty($taxi['success'])){
				$campaigns = $this->getTaxiCampaigns((string)$taxi['data']['_id']);
		        if(!empty($_GET['data'])){
					$data = $_GET['data'];
					$clean_data = unserialize(gzuncompress(stripslashes(base64_decode(strtr($data, '-_,', '+/=')))));
					if(!empty($clean_data)){
						foreach($clean_data as $key => $val){
							$check_cmp = $this->Campaign_Model->findFirstWhere(array('unique_hash' => $val['unique_hash']));
							if(!empty($check_cmp['success']) ){
								$rtn = $this->Campaign_Model->update($val, (String)$val['_id']);
							}else{
								$rtn = $this->Campaign_Model->save((Object)$val);
							}
						}
					}
				}else{
		            // update live campaign data
					if(!empty($campaigns['success'])){
						$campaigns['data'] = reindex($campaigns['data'], 'unique_hash');
						$campaigns['data'] = strtr(base64_encode(addslashes(gzcompress(serialize($campaigns['data']),9))), '+/=', '-_,');
			            $send_array = http_build_query($campaigns);
						exit(redirect(base_url().'consolidate_live?'.$send_array, 'refresh'));
					}
		        }
			}
		}

		exit(redirect('campaigns'));
    }

	private function getTaxiCampaigns($taxi_id){
		$taxi_campaigns = $this->RouteTaxiCampaign_Model->findManyWhere(array('taxi_id' => $taxi_id);
		if(!empty($taxi_campaigns['success']) && !empty($taxi_campaigns['data'])){
			foreach($taxi_campaigns['data'] as &$taxi_campaign){
				$taxi_campaign_data = $this->Campaign_Model->findFirstWhere(array('_id' => new MongoDB\BSON\ObjectId($taxi_campaign['campaign_id']), 'status' => array('$ne' => 'Ended')));
				if(!empty($taxi_campaign_data['data'])){
					$taxi_campaign_data = $taxi_campaign_data['data'];
					$current_date = date('Y-m-d');

					$campaign_id = !empty($taxi_campaign_data['_id'])?(String)$taxi_campaign_data['_id']:'';
					$status = '';						// start campaign
					if($taxi_campaign_data['status'] == 'Pending' && !empty($taxi_campaign_data['start_date']) && strtotime($taxi_campaign_data['start_date']) >= strtotime($current_date)){
						$status = 'Active';
					}else{
						$status = $taxi_campaign_data['status'];
					}

					// end campaign
					if(!empty($taxi_campaign_data['start_date']) && strtotime($taxi_campaign_data['start_date']." + ".$taxi_campaign_data['campaign_duration']." weeks") <= strtotime($current_date)){
						$status = 'Ended';
					}
					if($status != $taxi_campaign_data['status']){
						$data = array( "status" => $status );
						$rtn = $this->Campaign_Model->update($data, $campaign_id);
						if(!empty($rtn['success'])){
							$taxi_campaign_data['status'] = $status;
						}
					}
					$taxi_campaign_data['taxi_data'] = (array)$taxi_campaign;
					$taxi_campaign = $taxi_campaign_data;
				}
			}

			return $taxi_campaigns;
		}else{
			return array();
		}
	}
}
