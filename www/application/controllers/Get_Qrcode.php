<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Endroid\QrCode\ErrorCorrectionLevel;
use Endroid\QrCode\LabelAlignment;
use Endroid\QrCode\QrCode;
use Symfony\Component\HttpFoundation\Response;

class Get_qrcode extends Session_bot {
	function __construct(){
		parent::__construct();
	}

	public function index()
	{
        require_once 'vendor/autoload.php';
		$link = !empty($_GET['link'])?$_GET['link']:'';
		$target = !empty($link)?$link:'No link provided.';
        $qrCode = new QrCode();

        $qrCode
            ->setSize(300)
            ->setText($target)
            ->setWriterByName('png')
            ->setMargin(10)
            // ->setPadding(10)
            ->setEncoding('UTF-8')
            ->setErrorCorrectionLevel(ErrorCorrectionLevel::HIGH)
            ->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0])
            ->setBackgroundColor(['r' => 238, 'g' => 238, 'b' => 238])
            // ->setLabel('Scan the code', 16, __DIR__.'/../assets/noto_sans.otf', LabelAlignment::CENTER)
            // ->setLogoPath(__DIR__.'/../assets/symfony.png')
            // ->setLogoWidth(150)
            ->setValidateResult(false);

        header('Content-Type: image/png');
        echo $qrCode->writeString();
	}
}
