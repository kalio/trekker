<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Consolidate_live extends Session_bot {

	private $errors = array();

	function __construct(){
		parent::__construct();
        if(!$this->isLoggedin()){
            redirect('login');
        }
	}

	public function index()	{
        $new_data = $_GET['data'];
		$new_data = unserialize(gzuncompress(stripslashes(base64_decode(strtr($new_data, '-_,', '+/=')))));
		$rtn_data_arr = $data_arr = array();
		if(!empty($new_data)){
            foreach($new_data as $key => &$campaign){
				$campaign_data = $this->Campaign_Model->findFirstWhere(array('_id' => $campaign['_id']));
				if(!empty($campaign_data['success'])){
					$campaign = array_merge($campaign_data['data'], $campaign);
					_d($campaign, 1);
					$campaign_id = !empty($campaign['_id'])?(String)$campaign['_id']:'';
					$data = array();
	                if(!empty($campaign['status']) && $campaign['status'] != 'Inactive' && !empty($new_data[$campaign['unique_hash']])){
	                    $current_date = date('Y-m-d');
	                    // end campaign
	                    if(!empty($campaigns['data'][$key]['start_date']) && strtotime($campaigns['data'][$key]['start_date']." + ".$campaign['campaign_duration']." weeks") <= strtotime($current_date)){
	                        $data['status'] = $new_data[$campaign['unique_hash']]['status'] = 'Ended';
	                    }

	                    // process time ellapsed
	                    if(!empty($campaign['status']) && $campaign['status'] == 'Active' && !empty($campaign['start_date'])){
	                        $time_ellapsed = strtotime($current_date) - strtotime($campaign['start_date']);
	                        $data['time_ellapsed'] = $new_data[$campaign['unique_hash']]['time_ellapsed'] = intval(intval($time_ellapsed)/(3600*24));
	                    }

						// Update slide count
						if(!empty($campaign['taxi_data'])){
							$campaign_taxi_data = $this->RouteTaxiCampaign_Model->findFirstWhere(array('campaign_id' => (string)$campaign['_id'], 'taxi_id' => $campaign['taxi_data']['taxi_id']));
							if(!empty($campaign_taxi_data['success'])){
								if(!empty($campaign_taxi_data['count'])){
									$data['count'] = $campaign_taxi_data['count'] + $campaign['taxi_data']['count'];
								}else{
									$data['count'] = $campaign['taxi_data']['count'];
								}
								$new_data[$campaign['unique_hash']]['count'] = 0;
							}
						}


						// update campaign
						$rtn = $this->Campaign_Model->update($data, $campaign_id);
						if($rtn['success'] == true){
							array_push($rtn_data_arr, $new_data[$campaign['unique_hash']]);
						}
	                }
				}
            }

			$rtn_data_arr = reindex($rtn_data_arr, 'unique_hash');
			foreach ($rtn_data_arr as $ind => $cmp) {
				if(!empty($cmp['status']) && ($cmp['status'] == 'Inactive' || $cmp['status'] == 'Ended')){
					unset($rtn_data_arr[$ind]);
				}
			}
			_d($rtn_data_arr, 1);
			$data_arr['data'] = strtr(base64_encode(addslashes(gzcompress(serialize($rtn_data_arr),9))), '+/=', '-_,');
            $data_arr = http_build_query($data_arr);
			exit(redirect(base_url().'consolidate?'.$data_arr, 'refresh'));

		}
    }
}
