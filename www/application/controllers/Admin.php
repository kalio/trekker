<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends Admin_base {
	function __construct(){
		parent::__construct();

        if(!$this->isLoggedin()){
            redirect('login');
        }
	}

	public function index()
	{
		$this->load->view('dashboard');
	}
}
