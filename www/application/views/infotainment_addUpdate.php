<?php include_once('dash_header.php');?>
<div class="main-content">
	<div class="container">
		<?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
		<?php if(!empty($validation_errors)):foreach ($validation_errors as $validation_error):?>
			<div class="alert alert-danger"><?php echo $validation_error; ?></div>
		<?php endforeach; endif; ?>
		<?php if(!empty($update_success)): ?>
			<div class="alert alert-success">
				The update is saved.
			</div>
		<?php endif;?>
		<div class="page-content">
			<div class="single-head">
				<h3 class="pull-left"><?php echo !empty($page_sub_title)?'<i class="fa fa-form lblue"></i> '.$page_sub_title:''; ?></h3>
				<div class="clearfix"></div>
			</div>
			<div class="page-form">
				<form class="form-horizontal" role="form" action="" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<div class="col-lg-12 form-group">
							<label class="col-lg-12 control-label">Title</label>
							<div class="col-lg-12">
								<input type="text" name="title" value="<?php echo !empty($post['title'])?$post['title']:(!empty($infotainment['title'])?$infotainment['title']:''); ?>" class="form-control" placeholder="">
							</div>
						</div>
						<div class="col-lg-12 form-group">
							<div class="col-lg-12" style="margin:50px 0px">
								<input type="file" name="video" style="display:none" id="video">
								<input type="text" class="hidden" name="video" value="<?php echo !empty($infotainment['video'])?$infotainment['video']:""; ?>">
								<input type="text" name="video_length" class="hidden" id="duration" value="<?php echo !empty($infotainment['video_length'])?$infotainment['video_length']:''; ?>">
								<video class="" id="video_tg" height="300" width="500" controls>
									<source src="<?php echo !empty($infotainment['video'])?base_url().'assets/infotainments/'.$infotainment['video']:'' ?>" type="video/mp4"></source>
								</video>
								<div style="clear:both"></div>
								<button type="button" class="btn bt-warning change_vid"><?php if(empty($infotainment)):?>Add video<?php else: ?>Update video<?php endif; ?></button>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-offset-1 col-lg-10">
							<button type="submit" type="button" class="btn btn-success">Save</button>
						</div>
					</div>
				</form>
			</div>
		</div>
    </div>
</div>
<?php include_once('dash_footer.php');?>
