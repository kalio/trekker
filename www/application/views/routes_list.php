<?php include_once('dash_header.php');?>
<div class="main-content">
	<div class="container">
        <div class="row">

            <div class="col-md-12">
                <!-- Pages widget -->
                <div class="widget pages-widget">

                    <div class="widget-head br-red">
                        <h3><i class="fa fa-file"></i> Routes List</h3>
                    </div>

                    <div class="widget-body no-padd">

                        <div class="table-responsive">
                            <?php if(!empty($routes)): ?>
                            <table class="table table-bordered">
                                <!-- Table heading -->
                                <tr>
                                    <th style="text-align:left">Route</th>
                                    <th>Available of Taxis</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            <?php foreach ($routes as $route): ?>
                                <tr>
                                    <td style="text-align:left"><span><?php echo !empty($route['route'])?ucfirst($route['route']):''; ?></span></td>
                                    <td><span><?php echo !empty($route['taxis'])?count($route['taxis']):0; ?></span></span></td>
                                    <td><span><?php echo !empty($route['status'])?$route['status']:''; ?></span></td>
                                    <td>
                                        <a href="<?php echo base_url().'routes/addUpdate?route_hash='.$route['unique_hash']; ?>"><i class="fa fa-edit lblue"></i></a> &nbsp;
                                        <!-- <a href="#"><i class="fa fa-share-square-o green"></i></a> &nbsp; -->
                                        <i class="fa fa-trash-o red delete_route" data-value="<?php echo base_url().'routes/delete?route_hash='.$route['unique_hash']; ?>"></i>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </table>
                            <?php else: ?>
                            <span>no Route added yet.</span>
                            <?php endif; ?>
                        </div>

                    </div>

                    <div class="widget-foot">
                        <span class="pull-left">
                            <a href="<?php echo base_url().'routes/addUpdate'; ?>" class="btn btn-info btn-sm">Add New Route</a> &nbsp;
                            <!-- <a href="#" class="btn btn-danger btn-sm">Delete All</a> -->
                        </span>
                        <div class="pull-right">
                            <!-- Pagination -->
                            <ul class="pagination">
                              <li><a href="#">&laquo;</a></li>
                              <li><a href="#">1</a></li>
                              <li><a href="#">2</a></li>
                              <li><a href="#">3</a></li>
                              <li><a href="#">5</a></li>
                              <li><a href="#">&raquo;</a></li>
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                </div>

            </div>

        </div>
    </div>
</div>
<?php include_once('dash_footer.php');?>
