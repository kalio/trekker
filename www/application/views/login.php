<!doctype html>
<html lang="en">
	<head>
       	<link type="text/css" href="<?php echo base_url().'assets/css/bootstrap.css'?>" rel="stylesheet"/>
       	<link type="text/css" href="<?php echo base_url().'assets/css/login.css'?>" rel="stylesheet"/>
	</head>
	<body style="margin:0px; padding: 0px;">
        <div class="container">

            <form class="form-signin" method="post" action="">
                <h2 class="form-signin-heading">Sign in</h2>
                <label for="inputEmail" class="sr-only">Email address</label>
                <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
                <label for="inputPassword" class="sr-only">Password</label>
                <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="remember-me"> Remember me
                    </label>
                </div>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            </form>

        </div>
    </body>
</html>
