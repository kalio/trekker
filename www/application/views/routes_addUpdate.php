<?php include_once('dash_header.php');?>
<div class="main-content">
	<div class="container">
		<?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
		<?php if(!empty($validation_errors)):foreach ($validation_errors as $validation_error):?>
			<div class="alert alert-danger"><?php echo $validation_error; ?></div>
		<?php endforeach; endif; ?>
		<?php if(!empty($update_success)): ?>
			<div class="alert alert-success">
				The update is saved.
			</div>
		<?php endif;?>
		<div class="page-content">
			<div class="single-head">
				<h3 class="pull-left"><?php echo !empty($page_sub_title)?'<i class="fa fa-form lblue"></i> '.$page_sub_title:''; ?></h3>
				<div class="clearfix"></div>
			</div>
			<div class="page-form">
				<form class="form-horizontal" role="form" action="" method="post" enctype="multipart/form-data">
                    <input name="id" value="<?php echo !empty($route)?(string)$route['_id']:''; ?>" class="hidden">
                    <div class="form-group" style="margin-bottom:20px!important">
                        <label class="col-lg-12 control-label">Route</label>
                        <div class="col-lg-12">
                            <input type="text" name="route" value="<?php echo !empty($post['route'])?$post['route']:(!empty($route['route'])?$route['route']:''); ?>" class="form-control" placeholder="">
                        </div>
					</div>
					<div class="form-group">
						<div class="col-lg-12">
							<button type="submit" type="button" class="btn btn-success" style="padding:6px 20px">Save</button>
						</div>
					</div>
				</form>
			</div>
		</div>
    </div>
</div>
<?php include_once('dash_footer.php');?>
