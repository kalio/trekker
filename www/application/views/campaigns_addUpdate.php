<?php include_once('dash_header.php');?>
<div class="main-content">
	<div class="container">
		<?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
		<?php if(!empty($validation_errors)):foreach ($validation_errors as $validation_error):?>
			<div class="alert alert-danger"><?php echo $validation_error; ?></div>
		<?php endforeach; endif; ?>
		<?php if(!empty($update_success)): ?>
			<div class="alert alert-success">
				The update is saved.
			</div>
		<?php endif;?>
		<div class="page-content">
			<div class="single-head">
				<h3 class="pull-left"><?php echo !empty($page_sub_title)?'<i class="fa fa-form lblue"></i> '.$page_sub_title:''; ?></h3>
				<div class="clearfix"></div>
			</div>
			<div class="page-form">
				<form class="form-horizontal" role="form" action="" method="post" enctype="multipart/form-data">
					<div class="col-lg-12 single-head">
						<h3 class="pull-left">Owner Info</h3>
						<div class="clearfix"></div>
					</div>
					<div class="form-group owner_name hidden owner_switch">
						<label class="col-lg-12 control-label">Owner</label>
						<div class="col-lg-12">
						<?php if(empty($campaign)):?>
							<input type="text" name="owner_search" value="<?php echo !empty($post['owner_search'])?$post['owner_search']:''; ?>" class="form-control owner_search" placeholder="First name, Last name">
							<div id="results"></div>
						<?php else:?>
							<label class="col-lg-2 control-label"><b><?php echo !empty($owner)?$owner['first_name'].' '.$owner['last_name']:''?></b></label>
						<?php endif; ?>
							<input type="hidden" name="user_id" id="user_id" value="<?php echo !empty($post['user_id'])?$post['user_id']:(!empty($campaign['owner'])?$campaign['owner']:''); ?>">
						</div>
					</div>
				<?php if(empty($campaign)):?>
					<div class="form-group">
						<div class="col-lg-12">
							<button type="button" class="btn btn-default toggle_owner owner_form" data-value="owner_name">Existing client</button>
							<button type="button" class="btn btn-info toggle_owner owner_name hidden" data-value="owner_form">New Client</button>
						</div>
					</div>
				<?php endif; ?>
					<div class="form-group owner_form owner_switch">
						<div class="col-lg-6 form-group">
							<label class="col-lg-12 control-label">First Name</label>
							<div class="col-lg-12">
								<input type="text" name="first_name" value="<?php echo !empty($post['first_name'])?$post['first_name']:''; ?>" class="form-control" placeholder="">
							</div>
						</div>
						<div class="col-lg-6 form-group">
							<label class="col-lg-12 control-label">Last Name</label>
							<div class="col-lg-12">
								<input type="text" name="last_name" value="<?php echo !empty($post['last_name'])?$post['last_name']:''; ?>" class="form-control" placeholder="">
							</div>
						</div>
						<div class="col-lg-6 form-group">
							<label class="col-lg-12 control-label">Email</label>
							<div class="col-lg-12">
								<input type="text" name="email" value="<?php echo !empty($post['email'])?$post['email']:''; ?>" class="form-control" placeholder="">
							</div>
						</div>
						<div class="col-lg-6 form-group">
							<label class="col-lg-12 control-label">Phone number</label>
							<div class="col-lg-12">
								<input type="text" name="phone_number" value="<?php echo !empty($post['phone_number'])?$post['phone_number']:''; ?>" class="form-control" placeholder="">
							</div>
						</div>
						<div class="col-lg-12 form-group">
							<label class="col-lg-12 control-label">Company/Organisation</label>
							<div class="col-lg-12">
								<input type="text" name="company" value="<?php echo !empty($post['company'])?$post['company']:''; ?>" class="form-control" placeholder="">
							</div>
						</div>
						<div class="col-lg-12 form-group">
							<label class="col-lg-12 control-label">Address</label>
							<div class="col-lg-12">
								<textarea type="text" name="address" class="form-control" placeholder=""><?php echo !empty($post['address'])?$post['address']:''; ?></textarea>
							</div>
						</div>
						<div class="col-lg-6 form-group">
							<label class="col-lg-12 control-label">Password</label>
							<div class="col-lg-12">
								<input type="password" name="password" class="form-control" placeholder="">
							</div>
						</div>
						<div class="col-lg-6 form-group">
							<label class="col-lg-12 control-label">Confirm Password</label>
							<div class="col-lg-12">
								<input type="password" name="confirm_password" class="form-control" placeholder="">
							</div>
						</div>
					</div>
					<div class="col-lg-12 single-head">
						<h3 class="pull-left">Campaign Info</h3>
						<div class="clearfix"></div>
					</div>
					<div class="form-group">
						<div class="col-lg-12 form-group">
							<label class="col-lg-12 control-label">Campaign Title</label>
							<div class="col-lg-12">
								<input type="text" name="campaign_title" value="<?php echo !empty($post['campaign_title'])?$post['campaign_title']:(!empty($campaign['campaign_title'])?$campaign['campaign_title']:''); ?>" class="form-control" placeholder="">
							</div>
						</div>
						<div class="col-lg-6 form-group">
							<label class="col-lg-12 control-label">Campaign duration</label>
							<div class="col-lg-12">
								<select name="campaign_duration" class="form-control">
									<option value="1" <?php if((!empty($post['campaign_duration']) && $post['campaign_duration'] == 1) || (!empty($campaign['campaign_duration']) && $campaign['campaign_duration'] == 1)): echo 'selected="selected"'; endif; ?>> 1 Week</option>
								<?php for($x=2; $x <= 54; $x++):?>
									<option value="<?php echo $x; ?>" <?php if((!empty($post['campaign_duration']) && $post['campaign_duration'] == $x) || (!empty($campaign['campaign_duration']) && $campaign['campaign_duration'] == $x)): echo 'selected="selected"'; endif; ?>> <?php echo $x; ?> Weeks</option>
								<?php endfor;?>
								</select>
							</div>
						</div>
						<div class="col-lg-6 form-group">
							<label class="col-lg-12 control-label">Campaign Start</label>
							<div class="col-lg-12">
								<input type="text" name="start_date" value="<?php echo !empty($post['start_date'])?$post['start_date']:(!empty($campaign['start_date'])?$campaign['start_date']:''); ?>" class="form-control datepicker" placeholder="">
							</div>
						</div>

						<div class="col-lg-12 <?php if(!empty($campaign['status']) && $campaign['status'] == 'Active'):?>hidden<?php endif;?>" style="padding:0px">
							<div class="col-lg-12 form-group">
								<label class="col-lg-12 control-label" style="text-align:left">Campaign Routes</label>
							</div>
							<div class="routes">
								<?php if(!empty($campaignroutestaxis)): $i = 0; foreach($campaignroutestaxis as $campaign_route): ?>
								<div class="route_box col-lg-12 form-group" style="padding: 0px 30px; margin-bottom:15px!important">
									<?php if($i > 0): ?><span class="btn btn-danger remove_route" data-index="<?php echo $i ?>">x</span><?php endif; ?>
									<div class="col-lg-6" style="padding:0px">
										<select name="" id="select_route_<?php echo $i; ?>" class="form-control select_route" data-index="<?php echo $i; ?>" placeholder="">
											<option value="">Select Route</option>
											<?php if(!empty($routes)) foreach($routes as $route): $route_id = (string)$route['_id']; ?>
											<option value="<?php echo $route_id; ?>" <?php if((!empty($post['routes'][$i]['route_id']) && ($post['routes'][$i]['route_id'] == $route_id)) || (empty($post['routes'][$i]['route_id']) && !empty($campaign_route['route_id']) && ($campaign_route['route_id'] == $route_id))){ echo 'selected="seleted"';} ?>><?php echo $route['route']; ?></option>
											<?php endforeach;?>
										</select>
										<input type="hidden" name="routes[<?php echo $i; ?>][route]" value="<?php echo !empty($post['routes'][$i]['route_id'])?$post['routes'][$i]['route_id']:(!empty($campaign_route['route_id'])?$campaign_route['route_id']:'')?>" id="route_id_<?php echo $i; ?>">
									</div>
									<div class="col-lg-6" style="padding:0px">
										<select name="routes[<?php echo $i; ?>][no_taxi]" class="form-control" id="select_taxi_<?php echo $i; ?>" data-value="<?php echo $campaign_route['taxi_count']; ?>" placeholder="" readonly="readonly">
											<option value="">Select Number of Taxis</option>
										<?php $y = 1; if(!empty($campaign_route['taxi_data'])) foreach($campaign_route['taxi_data'] as $taxi_data):?>
											<option value="<?php echo $y; ?>" <?php echo (!empty($post['routes'][$i]['no_taxi']) && intval($post['routes'][$i]['no_taxi']) == $y)?'selected="seleted"':((empty($post['routes'][$i]['no_taxi']) && !empty($campaign_route['taxi_count']) && intval($campaign_route['taxi_count']) == $y)?'selected="seleted"':'')?>><?php echo $y; ?> Taxi(s)</option>
										<?php $y++; endforeach;?>
										</select>
									</div>
								</div>
								<?php $i++; endforeach; else: ?>
								<div class="route_box col-lg-12 form-group" style="padding: 0px 30px; margin-bottom:15px!important">
									<div class="col-lg-6" style="padding:0px">
										<select name="" id="select_route_0" class="form-control select_route" data-index="0" placeholder="">
			                                <option value="">Select Route</option>
			                                <?php if(!empty($routes)) foreach($routes as $route): $route_id = (string)$route['_id']; ?>
			                                <option value="<?php echo $route_id; ?>" <?php if((!empty($post['routes'][0]['route_id']) && ($post['routes'][0]['route_id'] == $route_id)) || (empty($post['routes'][0]['route_id']) && !empty($campaign['routes'][0]['route_id']) && ($campaign['routes'][0]['route_id'] == $route_id))){ echo 'selected="seleted"';} ?>><?php echo $route['route']; ?></option>
			                                <?php endforeach;?>
			                            </select>
										<input type="hidden" name="routes[0][route]" value="" id="route_id_0">
									</div>
									<div class="col-lg-6" style="padding:0px">
										<select name="routes[0][no_taxi]" class="form-control" id="select_taxi_0" placeholder="" readonly="readonly">
			                                <option value="">Select Number of Taxis</option>
			                            </select>
									</div>
								</div>
								<?php endif; ?>
							</div>
							<div class="col-lg-12 form-group" style="padding: 0px 30px">
								<button type="button" class="btn bt-warning add_route">Add another route</button>
							</div>
						</div>
						<div class="col-lg-12 <?php if(!empty($campaign['status']) && $campaign['status'] != 'Active'):?>hidden<?php endif;?>">
							<div class="col-lg-12 form-group">
								<label class="col-lg-12 control-label" style="text-align:left">Campaign Routes</label>
							</div>
							<div class="routes">
							<?php if(!empty($campaignroutestaxis)): $i = 0; foreach($campaignroutestaxis as $campaign_route): ?>
								<div class="route_box col-lg-12 form-group" style="padding: 0px 30px; margin-bottom:15px!important">
									<div class="col-lg-6" style="padding:0px">
										<b><?php echo $campaign_route['route_name']; ?></b>
									</div>
									<div class="col-lg-6" style="padding:0px">
										<b><?php echo $campaign_route['taxi_count']; ?> Taxi(s)</b>
									</div>
								</div>
							<?php endforeach; endif; ?>
							</div>
						</div>
						<div class="col-lg-12" style="padding:0px">
							<div class="col-lg-12 form-group">
								<label class="col-lg-12 control-label" style="text-align:left">Campaign links</label>
							</div>
							<?php for($i=0; $i <= 3; $i++):?>
							<div class="col-lg-6 form-group" style="margin-bottom:15px!important">
								<div class="col-lg-12">
									<input type="text" name="links[<?php echo $i; ?>][title]" value="<?php echo !empty($post['links'][$i]['title'])?$post['links'][$i]['title']:(!empty($campaign['links'][$i]['title'])?$campaign['links'][$i]['title']:''); ?>" class="form-control" placeholder="Link title">
								</div>
								<div class="col-lg-12">
									<input type="text" name="links[<?php echo $i; ?>][url]" value="<?php echo !empty($post['links'][$i]['url'])?$post['links'][$i]['url']:(!empty($campaign['links'][$i]['url'])?$campaign['links'][$i]['url']:''); ?>" class="form-control" placeholder="URL">
								</div>
							</div>
							<?php endfor; ?>
						</div>
						<div class="col-lg-12 form-group">
							<div class="col-lg-12" style="margin:50px 0px">
								<input type="file" name="campaign_video" style="display:none" id="campaign_video">
								<input type="text" class="hidden" name="campaign_video" value="<?php echo !empty($campaign['campaign_video'])?$campaign['campaign_video']:""; ?>">
								<input type="text" name="video_length" class="hidden" id="duration" value="<?php echo !empty($campaign['video_length'])?$campaign['video_length']:''; ?>">
								<video class="" id="video_tg" height="300" width="500" controls>
									<source src="<?php echo !empty($campaign['campaign_video'])?base_url().'assets/ads/'.$campaign['campaign_video']:'' ?>" type="video/mp4"></source>
								</video>
								<div style="clear:both"></div>
								<button type="button" class="btn bt-warning change_vid"><?php if(empty($campaign)):?>Add video<?php else: ?>Update video<?php endif; ?></button>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-12">
							<button type="submit" type="button" class="btn btn-success" style="padding:6px 20px">Save</button>
						</div>
					</div>
				</form>
			</div>
		</div>
    </div>
</div>
<div class="route_hold hidden">
	<div class="route_box col-lg-12 form-group" style="padding: 0px 30px; margin-bottom:15px!important">
		<span class="btn btn-danger remove_route">x</span>
		<div class="col-lg-6" style="padding:0px">
			<select name="" class="form-control select_route route" placeholder="">
				<option value="">Select Route</option>
				<?php if(!empty($routes)) foreach($routes as $route): $route_id = (string)$route['_id']; ?>
				<option value="<?php echo $route_id; ?>"><?php echo $route['route']; ?></option>
				<?php endforeach;?>
			</select>
		</div>
		<div class="col-lg-6" style="padding:0px">
			<select name="" class="form-control no_taxi" id="" placeholder="" readonly="readonly">
				<option value="">Select Number of Taxis</option>
			</select>
		</div>
	</div>
</div>
<?php include_once('dash_footer.php');?>
