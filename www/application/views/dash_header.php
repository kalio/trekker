
<!DOCTYPE html>
<html>
	<head>
		<!-- Title here -->
		<title><?php echo !empty($page_tile)?$page_tile:'Trekker Metro'; ?><?php echo !empty($page_sub_title)?' - '.$page_sub_title:''; ?></title>
		<!-- Description, Keywords and Author -->
		<meta name="description" content="Your description">
		<meta name="keywords" content="Your,Keywords">
		<meta name="author" content="ResponsiveWebInc">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Styles -->
		<!-- Bootstrap CSS -->
		<link href="<?php echo base_url()?>assets/css/bootstrap.css" rel="stylesheet">
		<!-- jQuery gritter -->
		<link href="<?php echo base_url()?>assets/css/jquery.gritter.css" rel="stylesheet">
		<!-- JQuery UI -->
		<link href="<?php echo base_url()?>assets/css/jquery-ui.css" rel="stylesheet">
		<!-- Font awesome CSS -->
		<link href="<?php echo base_url()?>assets/css/font-awesome.css" rel="stylesheet">
		<!-- Custom Color CSS -->
		<link href="<?php echo base_url()?>assets/css/less-style.css" rel="stylesheet">
		<!-- Custom CSS -->
		<link href="<?php echo base_url()?>assets/css/style.css" rel="stylesheet">
		<link href="<?php echo base_url()?>assets/css/custom.css" rel="stylesheet">

		<!-- Favicon -->
		<link rel="shortcut icon" href="#">
		<script>
			var Usersdata = <?php echo !empty($usersArray)?json_encode($usersArray):'{}' ?>;
		</script>
	</head>

	<body>

		<div class="outer">

			<!-- Sidebar starts -->
			<div class="sidebar">

				<div class="sidey">

					<!-- Logo -->
					<div class="logo">
						<h1><a href="admin"><i class="fa fa-desktop br-red"></i> Trekker <span>Metro</span></a></h1>
					</div>

					<!-- Sidebar navigation starts -->

					<!-- Responsive dropdown -->
					<div class="sidebar-dropdown"><a href="#" class="br-red"><i class="fa fa-bars"></i></a></div>

					<div class="side-nav">

						<div class="side-nav-block">
							<!-- Sidebar heading -->
							<h4>Menu</h4>
							<!-- Sidebar links -->
							<ul class="list-unstyled">
								<li><a href="<?php echo base_url()?>admin" class="<?php echo (!empty($this->uri->segment(1)) && $this->uri->segment(1) == 'admin')?'active':''?>"><i class="fa fa-desktop"></i> Dashboard</a></li>
								<li class="has_submenu <?php echo (!empty($this->uri->segment(1))&& $this->uri->segment(1) == 'campaigns')?'open':''?>">
									<a href="#"><i class="fa fa-file"></i> Campaigns <span class="nav-caret fa fa-caret-down"></span></a>
									<!-- Submenu -->
									<ul class="list-unstyled" style="<?php echo (!empty($this->uri->segment(1))&& $this->uri->segment(1) == 'campaigns')?'display:block;':''?>">
										<li><a href="<?php echo base_url()?>campaigns" class="<?php echo (empty($this->uri->segment(2)) && (!empty($this->uri->segment(1))&& $this->uri->segment(1) == 'campaigns'))?'active':''?>"><i class="fa fa-angle-double-right"></i> List Campaigns</a></li>
										<li><a href="<?php echo base_url()?>index.php/campaigns/addUpdate" class="<?php echo ((!empty($this->uri->segment(1))&& $this->uri->segment(1) == 'campaigns') && (!empty($this->uri->segment(2)) && $this->uri->segment(2) == 'addUpdate'))?'active':''?>"><i class="fa fa-angle-double-right"></i> Add Campaign</a></li>
										<li><a href="<?php echo base_url()?>index.php/campaigns/report" class="<?php echo ((!empty($this->uri->segment(1))&& $this->uri->segment(1) == 'campaigns') && (!empty($this->uri->segment(2)) && $this->uri->segment(2) == 'report'))?'active':''?>"><i class="fa fa-angle-double-right"></i> Report</a></li>
									</ul>
								</li>
								<li class="has_submenu <?php echo (!empty($this->uri->segment(1))&& $this->uri->segment(1) == 'routes')?'open':''?>">
									<a href="#"><i class="fa fa-file"></i> Routes <span class="nav-caret fa fa-caret-down"></span></a>
									<!-- Submenu -->
									<ul class="list-unstyled" style="<?php echo (!empty($this->uri->segment(1))&& $this->uri->segment(1) == 'routes')?'display:block;':''?>">
										<li><a href="<?php echo base_url()?>routes" class="<?php echo (empty($this->uri->segment(2)) && (!empty($this->uri->segment(1))&& $this->uri->segment(1) == 'routes'))?'active':''?>"><i class="fa fa-angle-double-right"></i> List Routes</a></li>
										<li><a href="<?php echo base_url()?>index.php/routes/addUpdate" class="<?php echo ((!empty($this->uri->segment(1))&& $this->uri->segment(1) == 'routes') && (!empty($this->uri->segment(2)) && $this->uri->segment(2) == 'addUpdate'))?'active':''?>"><i class="fa fa-angle-double-right"></i> Add Route</a></li>
									</ul>
								</li>
								<li class="has_submenu <?php echo (!empty($this->uri->segment(1))&& $this->uri->segment(1) == 'taxis')?'open':''?>">
									<a href="#"><i class="fa fa-file"></i> Taxis <span class="nav-caret fa fa-caret-down"></span></a>
									<!-- Submenu -->
									<ul class="list-unstyled" style="<?php echo (!empty($this->uri->segment(1))&& $this->uri->segment(1) == 'taxis')?'display:block;':''?>">
										<li><a href="<?php echo base_url()?>taxis" class="<?php echo (empty($this->uri->segment(2)) && (!empty($this->uri->segment(1))&& $this->uri->segment(1) == 'taxis'))?'active':''?>"><i class="fa fa-angle-double-right"></i> Taxis List</a></li>
										<li><a href="<?php echo base_url()?>index.php/taxis/addUpdate" class="<?php echo ((!empty($this->uri->segment(1))&& $this->uri->segment(1) == 'taxis') && (!empty($this->uri->segment(2)) && $this->uri->segment(2) == 'addUpdate'))?'active':''?>"><i class="fa fa-angle-double-right"></i> Add Taxi</a></li>
									</ul>
								</li>
								<li class="has_submenu <?php echo (!empty($this->uri->segment(1))&& $this->uri->segment(1) == 'infotainment')?'open':''?>">
									<a href="#"><i class="fa fa-file"></i> Infotainments <span class="nav-caret fa fa-caret-down"></span></a>
									<!-- Submenu -->
									<ul class="list-unstyled" style="<?php echo (!empty($this->uri->segment(1))&& $this->uri->segment(1) == 'infotainment')?'display:block;':''?>">
										<li><a href="<?php echo base_url()?>infotainment" class="<?php echo (empty($this->uri->segment(2)) && (!empty($this->uri->segment(1))&& $this->uri->segment(1) == 'infotainment'))?'active':''?>"><i class="fa fa-angle-double-right"></i> List Infotainments</a></li>
										<li><a href="<?php echo base_url()?>index.php/infotainment/addUpdate" class="<?php echo ((!empty($this->uri->segment(1))&& $this->uri->segment(1) == 'infotainment') && (!empty($this->uri->segment(2)) && $this->uri->segment(2) == 'addUpdate'))?'active':''?>"><i class="fa fa-angle-double-right"></i> Add Infotainment</a></li>
									</ul>
								</li>
							</ul>
						</div>
					</div>

					<!-- Sidebar navigation ends -->

				</div>
			</div>
			<!-- Sidebar ends -->

			<!-- Mainbar starts -->
			<div class="mainbar">

				<!-- Mainbar head starts -->
				<div class="main-head">
					<div class="container">
						<div class="row">
							<div class="col-md-3 col-sm-4 col-xs-6">
								<!-- Page title -->
								<h2><?php echo !empty($page_tile)?'<i class="fa fa-caret-right lblue"></i> '.$page_tile:''; ?></h2>
							</div>

							<div class="col-md-3 col-sm-4 col-xs-6">
								<!-- Search block -->
								<div class="head-search">
									<form class="form" role="form">
										<div class="input-group">
										  <input type="text" class="form-control" placeholder="Search...">
										  <span class="input-group-btn">
											<button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
										  </span>
										</div>
									</form>
								</div>
							</div>

							<div class="col-md-3 col-sm-4 hidden-xs">
								<!-- Notifications -->
								<div class="head-noty text-center">

									  <!-- Notifications -->
									  <div class="dropdown">
										  <a href="#" id="notifications" data-toggle="dropdown" class="mhead-icon" ><i class="fa fa-bell"></i> <span class="label label-info">5</span></a>
										  <!-- Dropdown -->
										  <ul class="dropdown-menu" role="button" aria-labelledby="notifications">

											<!-- Dropdown menu head -->
											<li class="dropdown-head">
												Notifications <span class="label label-info pull-right">5</span>
											</li>
											<!-- Content -->
											<li><a href="#">
												<!-- Colored icon with text and time -->
												<i class="fa fa-user"></i> New User Registered <b class="pull-right">5 mins ago</b>
												<div class="clearfix"></div>
											</a></li>
											<li><a href="#">
												<i class="fa fa-shopping-cart"></i> New Order for Dress <b class="pull-right">10 mins ago</b>
												<div class="clearfix"></div>
											</a></li>
											<li><a href="#">
												<i class="fa fa-sign-out"></i> User Ravi Kumar Sign Out <b class="pull-right">20 hr ago</b>
												<div class="clearfix"></div>
											</a></li>
											<li><a href="#">
												<i class="fa fa-envelope"></i> New Message from Ravi <b class="pull-right">1 day ago</b>
												<div class="clearfix"></div>
											</a></li>
											<li><a href="#">
												<i class="fa fa-laptop"></i> Full Backup Completed <b class="pull-right">2 day ago</b>
												<div class="clearfix"></div>
											</a></li>

											<li class="dropdown-foot text-center">
												<a href="#">View All</a>
											</li>

										  </ul>
									  </div>

									  <!-- Messages -->
									  <div class="dropdown">
										  <a href="#" id="messages" data-toggle="dropdown" class="mhead-icon" ><i class="fa fa-envelope"></i> <span class="label label-danger">5</span></a>
										  <ul class="dropdown-menu" role="button" aria-labelledby="messages">

											<!-- Dropdown menu head -->
											<li class="dropdown-head">
												Messages <span class="label label-danger pull-right">5</span>
											</li>
											<!-- Content -->
											<li><a href="#">
												<!-- Title -->
												Hello How are you? <i class="fa fa-reply pull-right"></i>
												<!-- Para -->
												<span>Lorem ipsum dolor sit amet something elit.</span>
												<div class="clearfix"></div>
											</a></li>
											<li><a href="#">
												What are you doing today? <i class="fa fa-reply pull-right"></i>
												<span>Lorem ipsum dolor sit amet something elit.</span>
												<div class="clearfix"></div>
											</a></li>
											<li><a href="#">
												Why don't you come here? <i class="fa fa-reply pull-right"></i>
												<span>Lorem ipsum dolor sit amet something elit.</span>
												<div class="clearfix"></div>
											</a></li>

											<li class="dropdown-foot text-center">
												<a href="#">View All</a>
											</li>

										  </ul>
									  </div>

									  <!-- Users -->
									  <div class="dropdown">
										  <a href="#" id="users" data-toggle="dropdown" class="mhead-icon" ><i class="fa fa-user"></i> <span class="label label-success">5</span></a>
										  <!-- Dropdown -->
										  <ul class="dropdown-menu drop-users" role="button" aria-labelledby="users">

											<!-- Dropdown menu head -->
											<li class="dropdown-head">
												Users <span class="label label-success pull-right">5</span>
											</li>
											<!-- Content -->
											<li><a href="#">
												<!-- User image -->
												<img src="img/user1.png" alt="" class="img-responsive img-circle" />
												<!-- User details -->
												<div class="duser-content">
													Anjelina Jolie
													<span>Lorem ipsum dolor sit amet elit.</span>
												</div>
												<div class="clearfix"></div>
											</a></li>
											<li><a href="#">
												<img src="img/user2.png" alt="" class="img-responsive img-circle" />
												<div class="duser-content">
													Brad Pitt
													<span>Lorem ipsum dolor sit amet elit.</span>
												</div>
												<div class="clearfix"></div>
											</a></li>
											<li><a href="#">
												<img src="img/user1.png" alt="" class="img-responsive img-circle" />
												<div class="duser-content">
													Al Pacino
													<span>Lorem ipsum dolor sit amet elit.</span>
												</div>
												<div class="clearfix"></div>
											</a></li>

											<li class="dropdown-foot text-center">
												<a href="#">View All</a>
											</li>

										  </ul>
									  </div>

								</div>
								<div class="clearfix"></div>
							</div>


							<div class="col-md-3 hidden-sm hidden-xs">
								<!-- Head user -->
								<div class="head-user dropdown pull-right">
									<a href="#" data-toggle="dropdown" id="profile">
										<!-- Icon
										<i class="fa fa-user"></i>  -->

										<img src="img/user2.png" alt="" class="img-responsive img-circle"/>

										<!-- User name -->
										ashokram <span class="label label-danger">5</span>
										<i class="fa fa-caret-down"></i>
									</a>
									<!-- Dropdown -->
									<ul class="dropdown-menu" aria-labelledby="profile">
									    <li><a href="#">View/Edit Profile <span class="badge badge-info pull-right">6</span></a></li>
										<li><a href="#">Change Settings</a></li>
										<li><a href="#">Messages <span class="badge badge-danger pull-right">5</span></a></li>
										<li><a href="#">Sign Out</a></li>
									</ul>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>

				</div>

				<!-- Mainbar head ends -->
