
			</div>
			<!-- Mainbar ends -->

			<div class="clearfix"></div>
		</div>

		<!-- Javascript files -->
	<!-- jQuery -->
		<script src="<?php echo base_url()?>assets/js/lib/js/jquery-3.2.1.min.js"></script>
		<!-- Bootstrap JS -->
		<script src="<?php echo base_url()?>assets/js/lib/js/bootstrap.js"></script>
		<!-- jQuery UI -->
		<script src="<?php echo base_url()?>assets/js/lib/js/jquery-ui.js"></script>
		<!-- Date Time Picker JS -->
		<script src="<?php echo base_url()?>assets/js/lib/js/jquery-datepicker.js"></script>
		<!-- Bootstrap wysihtml5 JS -->
		<script src="<?php echo base_url()?>assets/js/lib/js/wysihtml5-0.3.0.js"></script>
		<script src="<?php echo base_url()?>assets/js/lib/js/prettify.js"></script>
		<script src="<?php echo base_url()?>assets/js/lib/js/bootstrap-wysihtml5.min.js"></script>

		<!-- Validate JS -->
		<script src="<?php echo base_url()?>assets/js/lib/js/jquery.validate.js"></script>
		<!-- Form wizard steps  JS -->
		<script src="<?php echo base_url()?>assets/js/lib/js/jquery.steps.min.js"></script>
		<!-- jQuery Knob -->
		<script src="<?php echo base_url()?>assets/js/lib/js/jquery.knob.js"></script>
		<!-- jQuery Sparklines -->
		<script src="<?php echo base_url()?>assets/js/lib/js/jquery.sparklines.js"></script>
		<!-- jQuery slim scroll -->
		<script src="<?php echo base_url()?>assets/js/lib/js/jquery.slimscroll.min.js"></script>
		<!-- Data Tables JS -->
		<script src="<?php echo base_url()?>assets/js/lib/js/jquery.dataTables.min.js"></script>
		<!-- Pretty Photo JS -->
		<script src="<?php echo base_url()?>assets/js/lib/js/jquery.prettyPhoto.js"></script>
		<!-- Rate It JS -->
		<script src="<?php echo base_url()?>assets/js/lib/js/jquery.rateit.min.js"></script>
		<!-- Full calendar -->
	    <script src="<?php echo base_url()?>assets/js/lib/js/moment.min.js"></script>
	    <script src="<?php echo base_url()?>assets/js/lib/js/fullcalendar.min.js"></script>
		<!-- jQuery gritter -->
		<script src="<?php echo base_url()?>assets/js/lib/js/jquery.gritter.min.js"></script>
		<!-- jQuery gritter notification -->
		<script src="<?php echo base_url()?>assets/js/lib/js/custom.notification.js"></script>
		<!-- Respond JS for IE8 -->
		<script src="<?php echo base_url()?>assets/js/lib/js/respond.min.js"></script>
		<!-- HTML5 Support for IE -->
		<script src="<?php echo base_url()?>assets/js/lib/js/html5shiv.js"></script>
		<!-- Custom JS -->
		<script src="<?php echo base_url()?>assets/js/app/base.js"></script>
		<?php if(!empty($page)): ?><script src="<?php echo base_url()?>assets/js/app/<?php echo $page; ?>.js"></script><?php endif; ?>

	</body>
</html>
