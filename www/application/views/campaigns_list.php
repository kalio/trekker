<?php include_once('dash_header.php');?>
<div class="main-content">
	<div class="container">
        <div class="row">

            <div class="col-md-12">
                <!-- Pages widget -->
                <div class="widget pages-widget">

                    <div class="widget-head br-red">
                        <h3><i class="fa fa-file"></i> Campaign List</h3>
                    </div>

                    <div class="widget-body no-padd">

                        <div class="table-responsive">
                            <?php if(!empty($campaigns)): ?>
                            <table class="table table-bordered">
                                <!-- Table heading -->
                                <tr>
                                    <th style="text-align:left">Camapign</th>
                                    <th>Campaign Duration</th>
                                    <th>Time Elapsed</th>
                                    <th>Video Length</th>
                                    <th>Times Shown</th>
                                    <th>Slot Allocation</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            <?php foreach ($campaigns as $campaign): ?>
                                <tr>
                                    <td style="text-align:left"><span><?php echo !empty($campaign['campaign_title'])?ucfirst($campaign['campaign_title']):''; ?></span></td>
                                    <td><span><?php echo !empty($campaign['campaign_duration'])?$campaign['campaign_duration'].' week(s)':''; ?></span></span></td>
                                    <td><span><?php echo !empty($campaign['time_ellapsed'])?$campaign['time_ellapsed'].' day(s)':''; ?></span></td>
                                    <td><span><?php echo !empty($campaign['video_length'])?$campaign['video_length'].'s':''; ?></span></td>
                                    <td><span><?php echo !empty($campaign['count'])?$campaign['count']:0; ?></span></td>
                                    <td><span><?php echo !empty($campaign['slot_allocation'])?$campaign['slot_allocation']:''; ?></span></td>
                                    <td><span><?php echo !empty($campaign['status'])?$campaign['status']:''; ?></span></td>
                                    <td>
                                        <a href="<?php echo base_url().'campaigns/addUpdate?campaign_hash='.$campaign['unique_hash']; ?>"><i class="fa fa-edit lblue"></i></a> &nbsp;
                                        <!-- <a href="#"><i class="fa fa-share-square-o green"></i></a> &nbsp; -->
                                        <i class="fa fa-trash-o red delete_camp" data-value="<?php echo base_url().'campaigns/delete?campaign_hash='.$campaign['unique_hash']; ?>"></i>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </table>
                            <?php else: ?>
                            <span>no ads added yet.</span>
                            <?php endif; ?>
                        </div>

                    </div>

                    <div class="widget-foot">
                        <span class="pull-left">
                            <a href="<?php echo base_url().'campaigns/addUpdate'; ?>" class="btn btn-info btn-sm">Add New Campaign</a> &nbsp;
                            <!-- <a href="#" class="btn btn-danger btn-sm">Delete All</a> -->
                        </span>
                        <div class="pull-right">
                            <!-- Pagination -->
                            <ul class="pagination">
                              <li><a href="#">&laquo;</a></li>
                              <li><a href="#">1</a></li>
                              <li><a href="#">2</a></li>
                              <li><a href="#">3</a></li>
                              <li><a href="#">5</a></li>
                              <li><a href="#">&raquo;</a></li>
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                </div>

            </div>

        </div>
    </div>
</div>
<?php include_once('dash_footer.php');?>
