<!doctype html>
<html lang="en">
	<head>
       	<link type="text/css" href="<?php echo base_url().'assets/css/bootstrap.css'?>" rel="stylesheet"/>
       	<link type="text/css" href="<?php echo base_url().'assets/css/ad_board.css'?>" rel="stylesheet"/>
	</head>
	<body style="margin:0px; padding: 0px;">
		<input type="hidden" id="taxi_id" value="<?php echo !empty($taxi['_id'])?(string)$taxi['_id']:''; ?>">
		<div class="sidesshow" data-cycle-slides="> div">
			<div data-cycle-timeout="1"></div>
			<?php if(!empty($campaigns)):foreach($campaigns as $campaign): ?>
			<div class="use" data-cycle-timeout="<?php echo ($campaign['video_length'] * 1000)?>" data-hash="<?php echo $campaign['unique_hash']?>"  data-value="<?php echo (string)$campaign['_id']?>">
				<div class="vid_wrap">
				    <video class="vid">
					<?php if(!empty($campaign['campaign_video'])): ?>
				    	<source src="<?php echo base_url().'assets/ads/'.$campaign['campaign_video']; ?>" type="video/mp4"></source>
					<?php else: ?>
						<source src="<?php echo base_url().'assets/infotainments/'.$campaign['video']; ?>" type="video/mp4"></source>
					<?php endif; ?>
					</video>
				</div>
				<?php if(!empty($campaign['links'])): ?>
				<div class="base col-sm-12">
					<?php foreach($campaign['links'] as $link):?>
					<div class="base_item col-sm-3">
						<div class="base_item_inner">
							<img class="qr_img" src="get_qrcode?link=<?php echo !empty($link->url)?$link->url:'' ?>">
							<span class="qr_url"><?php echo !empty($link->title)?$link->title:'' ?></span>
						</div>
					</div>
					<?php endforeach;?>
				</div>
				<?php endif; ?>
			</div>
			<?php endforeach; endif; ?>
		</div>
    </body>
	<script src="<?php echo base_url().'assets/js/lib/js/jquery-3.2.1.min.js'?>"></script>
	<script src="<?php echo base_url().'assets/js/lib/js/cycle2-prod.js'?>"></script>
	<script src="<?php echo base_url().'assets/js/app/ad_board.js'?>"></script>
</html>
