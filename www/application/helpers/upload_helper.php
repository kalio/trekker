<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('upload'))
{
    function upload($file, $dest){
        $allowedExts = array("jpg", "jpeg", "gif", "png", "mp3", "mp4", "wma");
        $extension = pathinfo($file['name'], PATHINFO_EXTENSION);
        $return_array = array();
        $errors = array();
        if ((($file["type"] == "video/mp4")
        || ($file["type"] == "image/png")
        || ($file["type"] == "image/gif")
        || ($file["type"] == "image/jpeg"))
        && ($file["size"] < 10000000)
        && in_array($extension, $allowedExts)){
            if ($file["error"] > 0){
                array_push($errors, $file["error"]);
            }else{
                if(!is_dir(ASSPATH.$dest)){
                    mkdir(ASSPATH.$dest, 755, true);
                }
                $newFileName = generateRandomString();
                $size = round(($file["size"] / 1024/1024), 1);
                if($size < 10){
                    if (file_exists(ASSPATH.$dest.DIRECTORY_SEPARATOR. $newFileName.'.'.$extension)){
                        array_push($errors, "The File you are trying to upload already exists. ");
                    }else{
                        move_uploaded_file($file["tmp_name"], ASSPATH.$dest.DIRECTORY_SEPARATOR. $newFileName.'.'.$extension);
                        $return_array['success'] = true;
                        $data['file'] = $newFileName.'.'.$extension;
                    }
                }else{
                    array_push($errors, "The File you are trying to upload is too large, please upload a file less than 10Mb. ");
                }
            }
        }else{
            array_push($errors, "Invalid file");
        }

        if(!empty($return_array['success'])){
            $return_array['data'] = $data;
        }else{
            $return_array['success'] = false;
            $return_array['errors'] = $errors;
        }

        return $return_array;
    }

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
?>
