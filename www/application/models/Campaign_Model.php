<?php
class Campaign_Model extends  Trekker_Model {
	private $table_name = 'campaign';
	public $collection_fields = array(
		'owner' => array(
			'field' => 'owner',
			'label' => 'Owner',
			'rules' => 'trim'
		),
		'campaign_title' => array(
			'field' => 'campaign_title',
			'label' => 'Campaign Title',
			'rules' => 'trim|required'
		),
		'campaign_video' => array(
			'field' => 'campaign_video',
			'label' => 'Campaign Video',
			'rules' => 'trim'
		),
		'video_length' => array(
			'field' => 'video_length',
			'label' => 'Video Length',
			'rules' => 'trim'
		),
		'campaign_duration' => array(
			'field' => 'campaign_duration',
			'label' => 'Campaign Duration',
			'rules' => 'trim|required'
		),
		'slot_allocation' => array(
			'field' => 'slot_allocation',
			'label' => 'Slot Allocation',
			'rules' => 'trim'
		),
		'links' => array(
			'field' => 'links',
			'label' => 'Link(s)',
			'rules' => 'trim'
		)
	);

    function __construct(){
        parent::__construct(array('collection' => $this->table_name));
    }
}

?>
