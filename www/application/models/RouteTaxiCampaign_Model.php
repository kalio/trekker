<?php
class RouteTaxiCampaign_Model extends  Trekker_Model {
	private $table_name = 'route_taxi_campaigns';
	public $collection_fields = array(
		'campaign_id' => array(
			'field' => 'campaign_id',
			'label' => 'Campaign',
			'rules' => 'trim'
		),
		'route_id' => array(
			'field' => 'route_id',
			'label' => 'Route',
			'rules' => 'trim'
		),
		'taxis' => array(
			'field' => 'taxis',
			'label' => 'Taxi',
			'rules' => 'trim'
		),
		'count' => array(
			'field' => 'count',
			'label' => 'Count',
			'rules' => 'trim'
		),
		'status' => array(
			'field' => 'status',
			'label' => 'Status',
			'rules' => 'trim'
		)
	);

    function __construct(){
        parent::__construct(array('collection' => $this->table_name));
    }
}

?>
