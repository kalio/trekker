<?php
class Infotainment_Model extends  Trekker_Model {
	private $table_name = 'Infotainment';
	public $collection_fields = array(
		'title' => array(
			'field' => 'title',
			'label' => 'Title',
			'rules' => 'trim|required'
		),
		'video' => array(
			'field' => 'video',
			'label' => 'Video',
			'rules' => 'trim'
		),
		'video_length' => array(
			'field' => 'video_length',
			'label' => 'Video Length',
			'rules' => 'trim'
		)
	);

    function __construct(){
        parent::__construct(array('collection' => $this->table_name));
    }
}

?>
