<?php
class Taxi_Model extends  Trekker_Model {
	private $table_name = 'taxi';
	public $collection_fields = array(
		'taxi_number' => array(
			'field' => 'taxi_no',
			'label' => 'Taxi Number',
			'rules' => 'trim'
		),
        'taxi_license' => array(
			'field' => 'license',
			'label' => 'Taxi License Number',
			'rules' => 'trim|required'
		),
        'route' => array(
			'field' => 'route_id',
			'label' => 'Route',
			'rules' => 'trim|required'
		)
	);

    function __construct(){
        parent::__construct(array('collection' => $this->table_name));
    }
}

?>
