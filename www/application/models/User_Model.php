<?php
class User_Model extends  Trekker_Model {
	private $table_name = 'user';
	public $collection_fields = array(
		'first_name' => array(
			'field' => 'first_name',
			'label' => 'first name',
			'rules' => 'trim|required'
		),
		'last_name' => array(
			'field' => 'last_name',
			'label' => 'Last name',
			'rules' => 'trim|required'
		),
		'company' => array(
			'field' => 'company',
			'label' => 'Company/Organisation',
			'rules' => 'trim|required'
		),
		'email' => array(
			'field' => 'email',
			'label' => 'Email',
			'rules' => 'trim|required|valid_email'#|is_unique_in_collection[User.email]
		),
		'phone_number' => array(
			'field' => 'phone_number',
			'label' => 'Phone number',
			'rules' => 'trim|required|numeric|exact_length[11]'
		),
		'address' => array(
			'field' => 'address',
			'label' => 'Address',
			'rules' => 'trim'
		),
		'password' => array(
			'field' => 'password',
			'label' => 'Password',
			'rules' => 'trim'
		)
	);

    function __construct(){
        parent::__construct(array('collection' => $this->table_name));
    }
}

?>
