<?php
class Route_Model extends  Trekker_Model {
	private $table_name = 'routes';
	public $collection_fields = array(
		'route' => array(
			'field' => 'route',
			'label' => 'Route',
			'rules' => 'trim|required'
		)
	);

    function __construct(){
        parent::__construct(array('collection' => $this->table_name));
    }
}

?>
